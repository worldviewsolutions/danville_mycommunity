﻿dojo.require("esri.map");
dojo.require("dojo.fx");
dojo.require("esri.dijit.Popup");
dojo.require("esri.tasks.find");
dojo.require("esri.tasks.identify");
dojo.require("esri.tasks.locator");
dojo.require("esri.layers.FeatureLayer");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.AccordionContainer");
dojo.require("dijit.layout.AccordionPane");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.form.ToggleButton");
dojo.require("dijit.form.ComboBox");
dojo.require("dijit.form.FilteringSelect");
dojo.require("dijit.form.CheckBox");
dojo.require("dijit.form.RadioButton");
dojo.require("dijit.ProgressBar");
dojo.require("dijit.Dialog");
dojo.require("dojo.io.script");
dojo.require("dojo.data.ItemFileReadStore");

var map,
   popup,
   streetMapLayer,
   searchMapLayer,
   dynamicLayer,
   layerDefinition,
   clickPoint,
   connections = [],
   leBody,
   spatRef,
   searchTxt,
   rowList,
   wantStore,
   searchList,
   searchStore,
   websiteList = [],
   qTask,
   mapQuery,
   fparams,
   find,
   identify,
   polyId,
   polyParams,
   locate,
   lparams,
   locOutFields,
   idparams,
   legend,
   progress,

   wantITemp,
   locateITemp,

   servITemp,
   schITemp,
   bSITemp,
   trashITemp,
   pITemp,
   trailITemp,
   busITemp,
   cBTemp,
   eZTemp,
   fZTemp,
   histTemp,
   zTemp,
   rentTemp,
   hubTemp,
   parcITemp,

   wantSym,
   historSym,
   recCentSym,
   museSym,
   govSym,
   nonProfSym,
   libSym,
   postSym,
   airSym,
   trainSym,
   graveSym,
   pSSym,
   golfSym,
   fSSym,
   preSSym,
   eSSym,
   mSSym,
   hSSym,
   highEdSym,
   specSSym,
   privSSym,
   bSSym,
   trashSym,
   beautySym,
   repairSym,
   retailSym,
   shopSym,
   foodSym,
   contractSym,
   medSym,
   hotelSym,
   manufactSym,
   eduSSym,
   bankSym,
   finSym,
   recSym,
   otherSym,
   pSym,
   bTrailSym,
   hTrailSym,
   mTrailSym,
   cBGSym,
   cBSym,
   cTSym,
   eZSym,
   fSym,
   histSym,
   zSym,
   rentSym,
   hubSym,
   parcSym,

   wipeSchoolsIn,
   wipeSchoolsOut,
   wipeServIn,
   wipeServOut,
   wipeBusIn,
   wipeBusOut,
   wipeTrailsIn,
   wipeTrailsOut,
   wipeHistoricRowIn,
   wipeHistoricRowOut,
   wipeRecCentRowIn,
   wipeRecCentRowOut,
   wipeMuseRowIn,
   wipeMuseRowOut,
   wipeGovRowIn,
   wipeGovRowOut,
   wipeNonProfRowIn,
   wipeNonProfRowOut,
   wipeGolfRowIn,
   wipeGolfRowOut,
   wipeLibRowIn,
   wipeLibRowOut,
   wipePostRowIn,
   wipePostRowOut,
   wipeAirRowIn,
   wipeAirRowOut,
   wipeBusRowIn,
   wipeBusRowOut,
   wipeTrainRowIn,
   wipeTrainRowOut,
   wipeGraveRowIn,
   wipeGraveRowOut,
   wipeFStatRowIn,
   wipeFStatRowOut,
   wipePStatRowIn,
   wipePStatRowOut,
   wipeESRowIn,
   wipeESRowOut,
   wipeMSRowIn,
   wipeMSRowOut,
   wipeHSRowIn,
   wipeHSRowOut,
   wipeHighEdRowIn,
   wipeHighEdRowOut,
   wipeSpecSRowIn,
   wipeSpecSRowOut,
   wipePreSRowIn,
   wipePreSRowOut,
   wipePrivSRowIn,
   wipePrivSRowOut,
   wipeParkRowIn,
   wipeParkRowOut,
   wipeTrailRowIn,
   wipeTrailRowOut,
   wipeBeautyRowIn,
   wipeBeautyRowOut,
   wipeRepairRowIn,
   wipeRepairRowOut,
   wipeRetailRowIn,
   wipeRetailRowOut,
   wipeShopRowIn,
   wipeShopRowOut,
   wipeMedRowIn,
   wipeMedRowOut,
   wipeFoodRowIn,
   wipeFoodRowOut,
   wipeHotelRowIn,
   wipeHotelRowOut,
   wipeRecRowIn,
   wipeRecRowOut,
   wipeEduServRowIn,
   wipeEduServRowOut,
   wipeBankRowIn,
   wipeBankRowOut,
   wipeFinServRowIn,
   wipeFinServRowOut,
   wipeManufactRowIn,
   wipeManufactRowOut,
   wipeContractRowIn,
   wipeContractRowOut,
   wipeOtherRowIn,
   wipeOtherRowOut,
   wipeRentalRowIn,
   wipeRentalRowOut,

   historics = [],
   recCents = [],
   muses = [],
   govs = [],
   nonProfs = [],
   libs = [],
   posts = [],
   airs = [],
   trains = [],
   graves = [],
   pStats = [],
   golfs = [],
   shops = [],
   fStats = [],
   eSchools = [],
   mSchools = [],
   hSchools = [],
   specSchools = [],
   preSchools = [],
   highEds = [],
   privSchools = [],
   busResults = [],
   trashes = [],
   beauts = [],
   repairs = [],
   merches = [],
   foods = [],
   contracts = [],
   meds = [],
   hotels = [],
   manufacts = [],
   eduServs = [],
   banks = [],
   finances = [],
   recs = [],
   others = [],
   parks = [],
   trails = [],
   cBlocks = [],
   tracts = [],
   eZones = [],
   floods = [],
   hists = [],
   zones = [],
   rentals = [],
   hubs = [],

   searched,
   wanted,
   zoomed,
   uniqueID,
   servChildren,
   schoolChildren,
   busChildren,
   commChildren;

function toTitleCase(str) {
   var A = str.split(' '), B = [], C, D = [], E, F = [];
   for (var i = 0; A[i] !== undefined; i++) {
      C = A[i].split('.');
      if (C.length > 1) {
         for (var j = 0; C[j] !== undefined; j++) {
            D[D.length] = C[j].substr(0, 1).toUpperCase() + C[j].substr(1);
         }
         A[i] = D.join('.');
      }
      E = A[i].split('(');
      if (E.length > 1) {
         for (var j = 0; E[j] !== undefined; j++) {
            F[F.length] = E[j].substr(0, 1).toUpperCase() + E[j].substr(1);
         }
         A[i] = F.join('(');
      }
   }
   for (var i = 0; A[i] !== undefined; i++) {
      B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
   }
   return B.join(' ');
}

function init() {

   spatRef = new esri.SpatialReference(2284);

   //Create map and add the ArcGIS Online imagery layer
   var initialExtent = new esri.geometry.Extent({ "xmin": 11212705.811666667, "ymin": 3371380.585555555, "xmax": 11223844.700555556, "ymax": 3375547.252222222, "spatialReference": spatRef });
   popup = new esri.dijit.Popup({
      fillSymbol: new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 255, 0, 0.25]))
   }, dojo.create("div"));
   map = new esri.Map("mapDisplay", { infoWindow: popup, extent: initialExtent, logo: false, slider: "true" });
   dojo.connect(dijit.byId('mapDisplay'), 'resize', map, map.resize);


   streetMapLayer = new esri.layers.ArcGISTiledMapServiceLayer("http://gis.danville-va.gov/ArcGIS/rest/services/Caches/Roads_Cache/MapServer");
   dynamicLayer = new esri.layers.ArcGISDynamicMapServiceLayer("http://gis.danville-va.gov/ArcGIS/rest/services/Main/MapServer");
   searchMapLayer = new esri.layers.GraphicsLayer();
   dynamicLayer.setVisibleLayers([7]);
   map.addLayers([streetMapLayer, searchMapLayer, dynamicLayer]);

   defineToc();
   defineAnimations();
   defineWantList();
   defineWebsiteList();
   defineFeatureLayers();
   connectEvents();
   uniqueID = 0;

   params = new esri.tasks.FindParameters();
   params.returnGeometry = true;
   idparams = new esri.tasks.IdentifyParameters();
   idparams.returnGeometry = true;
   idparams.layerIds = [2];
   idparams.tolerance = 3;
   idparams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
   idparams.spatialReference = spatRef;
   polyParams = new esri.tasks.IdentifyParameters();
   polyParams.returnGeometry = false;
   polyParams.tolerance = 1;
   polyParams.spatialReference = spatRef;
   polyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
   find = new esri.tasks.FindTask("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer");
   locate = new esri.tasks.Locator("http://gis.danville-va.gov/ArcGIS/rest/services/Geocode_Address/GeocodeServer");
   identify = new esri.tasks.IdentifyTask("http://gis.danville-va.gov/ArcGIS/rest/services/Main/MapServer");
   polyId = new esri.tasks.IdentifyTask("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer");
   locOutFields = ["Shape", "Match_addr"];

   progress = new dijit.Dialog({
      style: "width: 300px",
      region: "center",
      draggable: false
   });
   progress.setContent('<div id="progressBar" data-dojo-type="dijit.ProgressBar" indeterminate="true" label="Loading Features" style="display: block" valign="middle"></div>');
   progress.show();

   searched = false;
   wanted = false;
   zoomed = false;
   searchList = [];
   defineInfoTemplates();
   defineRows();
}

function defineToc() {
   servChildren = [historic, recCent, muse, gov, nonProf, lib, post, air, train, grave, pStat, golf, fStat, edu, park, trail, bus];
   schoolChildren = [eSchool, mSchool, hSchool, highEd, specSchool, preSchool, privSchool];
   busChildren = [beauty, repair, retail, shop, med, food, hotel, rec, eduServ, bank, finServ, manufact, contract, other, rental];
}

function defineRows() {
   rowList = [
      historicRow, recCentRow, museRow,
      govRow, nonProfRow, golfRow, libRow, postRow, airRow, busRow,
      trainRow, graveRow, fStatRow, pStatRow,
      preSRow, eSRow, mSRow, hSRow, highEdRow, specSRow,
      privSRow, parkRow,
      trailRow, trashRow, beautyRow, repairRow,
      retailRow, shopRow, medRow, foodRow, hotelRow,
      recRow, eduServRow, bankRow, finServRow, manufactRow,
      contractRow, otherRow, rentalRow
   ];
}

function defineWantList() {
   wantStore = new dojo.data.ItemFileReadStore({
      url: "./wantList.json"
   });
   wantTo.set('store', wantStore);

   wantSym = new esri.symbol.PictureMarkerSymbol('blue-marker.png', 30, 30);
}

function defineWebsiteList() {
   qTask = new esri.tasks.QueryTask("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/49");
   mapQuery = new esri.tasks.Query();
   mapQuery.outFields = ["ITEM", "URL", "CATEGORY", "FEATURE", "FIELD"];
   mapQuery.where = "1=1";
   qTask.execute(mapQuery, function (results) {
      for (var i = 0; i < results.features.length; i++) {
         websiteList.push(results.features[i]);
      }
   });
}

function defineAnimations() {
   wipeSchoolsIn = new dojo.fx.wipeIn({
      node: schoolsNode,
      duration: 100
   });

   wipeSchoolsOut = new dojo.fx.wipeOut({
      node: schoolsNode,
      duration: 100
   });

   wipeServIn = new dojo.fx.wipeIn({
      node: servNode,
      duration: 100
   });

   wipeServOut = new dojo.fx.wipeOut({
      node: servNode,
      duration: 100
   });

   wipeBusIn = new dojo.fx.wipeIn({
      node: businessNode,
      duration: 100
   });

   wipeBusOut = new dojo.fx.wipeOut({
      node: businessNode,
      duration: 100
   });

   wipeTrailsIn = new dojo.fx.wipeIn({
      node: trailList,
      duration: 100
   });

   wipeTrailsOut = new dojo.fx.wipeOut({
      node: trailList,
      duration: 100
   });

   wipeHistoricRowIn = new dojo.fx.wipeIn({
      node: historicRow,
      duration: 5
   });

   wipeHistoricRowOut = new dojo.fx.wipeOut({
      node: historicRow,
      duration: 5
   });

   wipeRecCentRowIn = new dojo.fx.wipeIn({
      node: recCentRow,
      duration: 5
   });

   wipeRecCentRowOut = new dojo.fx.wipeOut({
      node: recCentRow,
      duration: 5
   });

   wipeMuseRowIn = new dojo.fx.wipeIn({
      node: museRow,
      duration: 5
   });

   wipeMuseRowOut = new dojo.fx.wipeOut({
      node: museRow,
      duration: 5
   });

   wipeGovRowIn = new dojo.fx.wipeIn({
      node: govRow,
      duration: 5
   });

   wipeGovRowOut = new dojo.fx.wipeOut({
      node: govRow,
      duration: 5
   });

   wipeNonProfRowIn = new dojo.fx.wipeIn({
      node: nonProfRow,
      duration: 5
   });

   wipeNonProfRowOut = new dojo.fx.wipeOut({
      node: nonProfRow,
      duration: 5
   });

   wipeLibRowIn = new dojo.fx.wipeIn({
      node: libRow,
      duration: 5
   });

   wipeLibRowOut = new dojo.fx.wipeOut({
      node: libRow,
      duration: 5
   });

   wipePostRowIn = new dojo.fx.wipeIn({
      node: postRow,
      duration: 5
   });

   wipePostRowOut = new dojo.fx.wipeOut({
      node: postRow,
      duration: 5
   });

   wipeAirRowIn = new dojo.fx.wipeIn({
      node: airRow,
      duration: 5
   });

   wipeAirRowOut = new dojo.fx.wipeOut({
      node: airRow,
      duration: 5
   });

   wipeBusRowIn = new dojo.fx.wipeIn({
      node: busRow,
      duration: 5
   });

   wipeBusRowOut = new dojo.fx.wipeOut({
      node: busRow,
      duration: 5
   });

   wipeTrainRowIn = new dojo.fx.wipeIn({
      node: trainRow,
      duration: 5
   });

   wipeTrainRowOut = new dojo.fx.wipeOut({
      node: trainRow,
      duration: 5
   });

   wipeGraveRowIn = new dojo.fx.wipeIn({
      node: graveRow,
      duration: 5
   });

   wipeGraveRowOut = new dojo.fx.wipeOut({
      node: graveRow,
      duration: 5
   });

   wipeFStatRowIn = new dojo.fx.wipeIn({
      node: fStatRow,
      duration: 5
   });

   wipeFStatRowOut = new dojo.fx.wipeOut({
      node: fStatRow,
      duration: 5
   });

   wipePStatRowIn = new dojo.fx.wipeIn({
      node: pStatRow,
      duration: 5
   });

   wipePStatRowOut = new dojo.fx.wipeOut({
      node: pStatRow,
      duration: 5
   });

   wipeESRowIn = new dojo.fx.wipeIn({
      node: eSRow,
      duration: 5
   });

   wipeESRowOut = new dojo.fx.wipeOut({
      node: eSRow,
      duration: 5
   });

   wipeMSRowIn = new dojo.fx.wipeIn({
      node: mSRow,
      duration: 5
   });

   wipeMSRowOut = new dojo.fx.wipeOut({
      node: mSRow,
      duration: 5
   });

   wipeHSRowIn = new dojo.fx.wipeIn({
      node: hSRow,
      duration: 5
   });

   wipeHSRowOut = new dojo.fx.wipeOut({
      node: hSRow,
      duration: 5
   });

   wipeHighEdRowIn = new dojo.fx.wipeIn({
      node: highEdRow,
      duration: 5
   });

   wipeHighEdRowOut = new dojo.fx.wipeOut({
      node: highEdRow,
      duration: 5
   });

   wipeSpecSRowIn = new dojo.fx.wipeIn({
      node: specSRow,
      duration: 5
   });

   wipeSpecSRowOut = new dojo.fx.wipeOut({
      node: specSRow,
      duration: 5
   });

   wipePreSRowIn = new dojo.fx.wipeIn({
      node: preSRow,
      duration: 5
   });

   wipePreSRowOut = new dojo.fx.wipeOut({
      node: preSRow,
      duration: 5
   });

   wipePrivSRowIn = new dojo.fx.wipeIn({
      node: privSRow,
      duration: 5
   });

   wipePrivSRowOut = new dojo.fx.wipeOut({
      node: privSRow,
      duration: 5
   });

   wipeParkRowIn = new dojo.fx.wipeIn({
      node: parkRow,
      duration: 5
   });

   wipeParkRowOut = new dojo.fx.wipeOut({
      node: parkRow,
      duration: 5
   });

   wipeGolfRowIn = new dojo.fx.wipeIn({
      node: golfRow,
      duration: 5
   });

   wipeGolfRowOut = new dojo.fx.wipeOut({
      node: golfRow,
      duration: 5
   });

   wipeTrailRowIn = new dojo.fx.wipeIn({
      node: trailRow,
      duration: 5
   });

   wipeTrailRowOut = new dojo.fx.wipeOut({
      node: trailRow,
      duration: 5
   });

   wipeBeautyRowIn = new dojo.fx.wipeIn({
      node: beautyRow,
      duration: 5
   });

   wipeBeautyRowOut = new dojo.fx.wipeOut({
      node: beautyRow,
      duration: 5
   });

   wipeRepairRowIn = new dojo.fx.wipeIn({
      node: repairRow,
      duration: 5
   });

   wipeRepairRowOut = new dojo.fx.wipeOut({
      node: repairRow,
      duration: 5
   });

   wipeRetailRowIn = new dojo.fx.wipeIn({
      node: retailRow,
      duration: 5
   });

   wipeRetailRowOut = new dojo.fx.wipeOut({
      node: retailRow,
      duration: 5
   });

   wipeShopRowIn = new dojo.fx.wipeIn({
      node: shopRow,
      duration: 5
   });

   wipeShopRowOut = new dojo.fx.wipeOut({
      node: shopRow,
      duration: 5
   });

   wipeMedRowIn = new dojo.fx.wipeIn({
      node: medRow,
      duration: 5
   });

   wipeMedRowOut = new dojo.fx.wipeOut({
      node: medRow,
      duration: 5
   });

   wipeFoodRowIn = new dojo.fx.wipeIn({
      node: foodRow,
      duration: 5
   });

   wipeFoodRowOut = new dojo.fx.wipeOut({
      node: foodRow,
      duration: 5
   });

   wipeHotelRowIn = new dojo.fx.wipeIn({
      node: hotelRow,
      duration: 5
   });

   wipeHotelRowOut = new dojo.fx.wipeOut({
      node: hotelRow,
      duration: 5
   });

   wipeRecRowIn = new dojo.fx.wipeIn({
      node: recRow,
      duration: 5
   });

   wipeRecRowOut = new dojo.fx.wipeOut({
      node: recRow,
      duration: 5
   });

   wipeEduServRowIn = new dojo.fx.wipeIn({
      node: eduServRow,
      duration: 5
   });

   wipeEduServRowOut = new dojo.fx.wipeOut({
      node: eduServRow,
      duration: 5
   });

   wipeBankRowIn = new dojo.fx.wipeIn({
      node: bankRow,
      duration: 5
   });

   wipeBankRowOut = new dojo.fx.wipeOut({
      node: bankRow,
      duration: 5
   });

   wipeFinServRowIn = new dojo.fx.wipeIn({
      node: finServRow,
      duration: 5
   });

   wipeFinServRowOut = new dojo.fx.wipeOut({
      node: finServRow,
      duration: 5
   });

   wipeManufactRowIn = new dojo.fx.wipeIn({
      node: manufactRow,
      duration: 5
   });

   wipeManufactRowOut = new dojo.fx.wipeOut({
      node: manufactRow,
      duration: 5
   });

   wipeContractRowIn = new dojo.fx.wipeIn({
      node: contractRow,
      duration: 5
   });

   wipeContractRowOut = new dojo.fx.wipeOut({
      node: contractRow,
      duration: 5
   });

   wipeOtherRowIn = new dojo.fx.wipeIn({
      node: otherRow,
      duration: 5
   });

   wipeOtherRowOut = new dojo.fx.wipeOut({
      node: otherRow,
      duration: 5
   });
   
   wipeRentalRowIn = new dojo.fx.wipeIn({
      node: rentalRow,
      duration: 5
   });   
   wipeRentalRowOut = new dojo.fx.wipeOut({
      node: rentalRow,
      duration: 5
   });
}

function defineFeatureLayers() {
   dojo.io.script.get({
      url: "http://gis.danville-va.gov/ArcGIS/rest/services/Main/MapServer/2?f=json",
      callbackParamName: "callback",
      load: function (result) {
         parcSym = new esri.symbol.SimpleFillSymbol(result.drawingInfo.renderer.symbol);
      },
      error: function () { alert("Unable to load Parcel Symbol"); }
   });

   dojo.io.script.get({
      url: "http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/layers?f=json",
      callbackParamName: "callback",
      load: function (result) {
         layerDefinition = result;
         defineSymbology();
      },
      error: function () { alert("Unable to define the symbology for the symbols."); }
   });

   dojo.io.script.get({
      url: "http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/legend?f=json",
      callbackParamName: "callback",
      load: function (result) {
         legend = result;
         defineTOCImages();
      },
      error: function () { alert("Unable to load the Table of Contents symbology."); }
   });
}

function defineSymbology() {
   for (var i = 0; i < layerDefinition.layers.length; i++) {
      if (layerDefinition.layers[i].name === 'Historic Spot') {
         historSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         historicImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Recreation Center') {
         recCentSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         recCentImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Museum') {
         museSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         museImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Government') {
         govSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         govImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Non-Profit') {
         nonProfSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         nonProfImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Library') {
         libSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         libImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Post Office') {
         postSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         postImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Airport') {
         airSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         airImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Bus Stop') {
         bSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         bSImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Train Station') {
         trainSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         trainImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Cemetery') {
         graveSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         graveImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Fire Station') {
         fSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         fStatImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Police Station') {
         pSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         pStatImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Preschool') {
         preSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         preSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Elementary') {
         eSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         eduImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
         eSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Middle') {
         mSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         mSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'High/Secondary') {
         hSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         hSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Higher Education') {
         highEdSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         highEdImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Specialty') {
         specSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         specSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Private') {
         privSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 26, 40);
         privSchoolImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Park') {
         if (layerDefinition.layers[i].drawingInfo.renderer.type == "simple") {
            pSym = new esri.symbol.SimpleFillSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         }
         else {
            pSym = new esri.symbol.PictureFillSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         }
      }
      if (layerDefinition.layers[i].name === 'Golf') {
         golfSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         golfImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Trails') {
         bTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[0].symbol);
         hTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[1].symbol);
         mTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[2].symbol);
      }
      if (layerDefinition.layers[i].name === 'Beauty/Barber') {
         beautySym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         beautyImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Repair') {
         repairSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         repairImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Retail') {
         retailSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         retailImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Shopping Center') {
         shopSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         shopImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Health Care') {
         medSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         medImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Restaurant') {
         foodSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         foodImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Room/Board') {
         hotelSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         hotelImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Recreation/Leisure') {
         recSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         recImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Education Services') {
         eduSSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         eduServImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Bank') {
         bankSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         bankImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Financial Services') {
         finSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         finServImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Manufacturing') {
         manufactSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         manufactImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Contractor') {
         contractSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         contractImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Other Services') {
         otherSym = new esri.symbol.PictureMarkerSymbol("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/" + i + "/images/" + layerDefinition.layers[i].drawingInfo.renderer.symbol.url, 28, 28);
         otherImg.src = "data:image/png;base64," + layerDefinition.layers[i].drawingInfo.renderer.symbol.imageData;
      }
      if (layerDefinition.layers[i].name === 'Rental Inspection Districts') {
         if (layerDefinition.layers[i].drawingInfo.renderer.type == "simple") {
            rentSym = new esri.symbol.SimpleFillSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         }
         else {
            rentSym = new esri.symbol.PictureFillSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         }
      }
   }
}

function defineTOCImages() {
   for (var i = 0, l = legend.layers.length; i < l; i++) {
      if (legend.layers[i].layerName == 'Park') {
         parkImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      }
      if (legend.layers[i].layerName == 'Trails') {
         trailImg.src = "data:image/png;base64," + legend.layers[i].legend[2].imageData;
         bikeTrl.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
         hikeTrl.src = "data:image/png;base64," + legend.layers[i].legend[1].imageData;
         walkTrl.src = "data:image/png;base64," + legend.layers[i].legend[2].imageData;
      }
   }
}

function connectEvents() {

   dojo.connect(searchBtn, "onClick", searchMap);
   dojo.connect(searchTxt, "onKeyDown", checkSearch);
   dojo.connect(clearBtn, "onClick", clearSearch);
   dojo.connect(wantTo, "onChange", getWant);
   dojo.connect(wantTo, "onClick", wantTo.loadDropDown);
   dojo.connect(map.infoWindow, "onHide", clearInfo);
   dojo.connect(checkAll, "onClick", checkLayers);
   dojo.connect(checkNone, "onClick", uncheckLayers);
   dojo.connect(servBtn, "onChange", wipeServ);
   servBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
   dojo.connect(businBtn, "onChange", wipeBus);
   businBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
   dojo.connect(schoolsBtn, "onChange", wipeSchools);
   dojo.connect(trlImgBtn, "onChange", wipeTrails);
   schoolsBtn.set('checked', false);
   trlImgBtn.set('checked', false);
   dojo.connect(serv, "onChange", changeServChildren);
   dojo.connect(edu, "onChange", changeEduChildren);
   dojo.connect(business, "onChange", changeBusChildren);
   dojo.connect(map, 'onClick', doParcelIdentify);

}

function openWantList() {
   wantTo.openDropDown();
}

function checkLayers() {
   serv.set('checked', true);
   business.set('checked', true);
   for (var i = 0; i < servChildren.length; i++) {
      servChildren[i].set('checked', true);
   }
   for (var i = 0; i < schoolChildren.length; i++) {
      schoolChildren[i].set('checked', true);
   }
   for (var i = 0; i < busChildren.length; i++) {
      busChildren[i].set('checked', true);
   }
}

function uncheckLayers() {
   serv.set('checked', false);
   business.set('checked', false);
   for (var i = 0; i < servChildren.length; i++) {
      servChildren[i].set('checked', false);
   }
   for (var i = 0; i < schoolChildren.length; i++) {
      schoolChildren[i].set('checked', false);
   }
   for (var i = 0; i < busChildren.length; i++) {
      busChildren[i].set('checked', false);
   }
}

function toggleCheckBox() {
   newCenterHandler();
}

function changeServChildren() {
   if (!serv.checked) {
      for (var i = 0, l = servChildren.length; i < l; i++) {
         servChildren[i].set('checked', false);
      }
   }
   else {
      for (var i = 0, l = servChildren.length; i < l; i++) {
         servChildren[i].set('checked', true);
      }
   }
   newCenterHandler();
}

function changeEduChildren() {
   if (!edu.checked || edu.disabled) {
      schoolsBtn.set('checked', false);
      for (var i = 0, l = schoolChildren.length; i < l; i++) {
         schoolChildren[i].set('checked', false);
      }
   }
   else {
      for (var i = 0, l = schoolChildren.length; i < l; i++) {
         schoolChildren[i].set('checked', true);
      }
   }
   newCenterHandler();
}

function changeBusChildren() {
   if (!business.checked) {
      for (var i = 0, l = busChildren.length; i < l; i++) {
         busChildren[i].set('checked', false);
      }
   }
   else {
      for (var i = 0, l = busChildren.length; i < l; i++) {
         busChildren[i].set('checked', true);
      }
   }
   newCenterHandler();
}

function wipeSchools() {
   if (schoolsBtn.checked) {
      schoolsBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
      wipeSchoolsIn.play();
   }
   else {
      schoolsBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoClosed');
      wipeSchoolsOut.play();
   }
}

function wipeServ() {
   if (servBtn.checked) {
      servBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
      wipeServIn.play();
   }
   else {
      servBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoClosed');
      wipeServOut.play();
   }
}

function wipeBus() {
   if (businBtn.checked) {
      businBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
      wipeBusIn.play();
   }
   else {
      businBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoClosed');
      wipeBusOut.play();
   }
}

function wipeTrails() {
   if (trlImgBtn.checked) {
      trlImgBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoOpened');
      wipeTrailsIn.play();
   }
   else {
      trlImgBtn.attr('class', 'tundra dijitTreeExpando dijitTreeExpandoClosed');
      wipeTrailsOut.play();
   }
}

function checkSearch(evt) {
   if (evt.keyCode === dojo.keys.ENTER) {
      searchMap();
   }
   else {
      if (evt.keyCode == dojo.keys.DELETE || evt.keyCode == dojo.keys.BACKSPACE) {
         searchTxt.closeDropDown();
      }
   }
}

function getWant() {
   if (wantTo.item != null) {
      wanted = true;
      searchMapLayer.clear();
      var point = new esri.geometry.Point(parseFloat(wantTo.item.x), parseFloat(wantTo.item.y), spatRef);
      if (wantTo.item.photo_url !== undefined) {
         wantITemp = new esri.InfoTemplate("&nbsp;", '<center><strong>' + wantTo.item.name + '</strong></center><table><tr><td><a href="' + wantTo.item.photo_url + '" target="_blank"><img src="' + wantTo.item.photo_url + '" width="50" height="50"/></a></td><td align="right" valign="top">' + wantTo.item.address + '<br/><a href="' + wantTo.item.webpage_url + '" target="_blank">Website</td></tr><tr><td colspan="2">' + wantTo.item.desc + '</td></tr></table>');
      }
      else {
         if (wantTo.item.webpage_url !== undefined) {
            wantITemp = new esri.InfoTemplate("&nbsp;", '<center><strong>' + wantTo.item.name + '</strong></center><table><tr><td align="left" valign="top">' + wantTo.item.address + '<br/><a href="' + wantTo.item.webpage_url + '" target="_blank">Website</td></tr><tr><td>' + wantTo.item.desc + '</td></tr></table>');
         }
         else {
            wantITemp = new esri.InfoTemplate("&nbsp", '<center><strong>' + wantTo.item.name + '</strong></center><table><tr><td align="left" valign="top">' + wantTo.item.address + '</td></tr><tr><td>' + wantTo.item.desc + '</td></tr></table>');
         }
      }
      var graphic = new esri.Graphic(point, wantSym, {}, wantITemp);
      map.graphics.add(graphic);
      map.infoWindow.setContent(graphic.getContent());
      map.infoWindow.setTitle(graphic.getTitle());
      map.infoWindow.show(point);
      map.centerAt(point);
   }
}

function getQuery(url, layerName, outputFields, qParam, returnGeometry, resultSet, searchFields, nextFunction) {
   var checkFeature = false;
   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === layerName) {
         checkFeature = true;
      }
   }
   qTask = new esri.tasks.QueryTask(url);
   mapQuery = new esri.tasks.Query();
   mapQuery.returnGeometry = returnGeometry;
   mapQuery.outFields = outputFields;
   mapQuery.where = qParam;
   qTask.execute(mapQuery, function (fSet) {
      for (var i = 0; i < fSet.features.length; i++) {
         resultSet.push(fSet.features[i]);
         if (checkFeature) {
            for (var j = 0; j < websiteList.length; j++) {
               if (fSet.features[i].attributes[websiteList[j].attributes.FIELD] != undefined) {
                  if (fSet.features[i].attributes[websiteList[j].attributes.FIELD] === websiteList[j].attributes.ITEM) {
                     resultSet[i].attributes.URL = websiteList[j].attributes.URL;
                  }
               }
            }
         }
      }
      for (var i = 0; i < resultSet.length; i++) {
         if (searchFields != null && searchFields != undefined) {
            for (var j = 0; j < searchFields.length; j++) {
               searchList.push({ val: resultSet[i].attributes[searchFields[j]] });
            }
         }
      }
   }, nextFunction).then(nextFunction);

   searchStore = new dojo.store.Memory({ data: searchList });
   searchTxt.set('store', searchStore);
}

function defineInfoTemplates() {
   servITemp = new esri.InfoTemplate("<br/>", '<strong>' + this.toTitleCase("${NAME}") + '</strong><br/> ${DESCRIPTIO}<br/><br/><strong>Address: </strong>${ADDRESS}<br/><a href="http://maps.google.com/maps?daddr=${ADDRESS}, Danville, VA&hl=en" target="_blank">Directions</a>');
   schITemp = new esri.InfoTemplate("<br/>", '<strong>${SCHOOL}</strong><br/>${TYPE}<br/><br/><strong>Address: </strong>${ADDRESS}<br/><a href="http://maps.google.com/maps?daddr=${ADDRESS}, Danville, VA&hl=en" target="_blank">Directions</a>');
   bSITemp = new esri.InfoTemplate("<br/>", '<strong>${ADDRESS}</strong><br/>${DESCRIPTION}<br/><a href="http://maps.google.com/maps?daddr=${ADDRESS}, Danville, VA&hl=en" target="_blank">Directions</a>');
   pITemp = new esri.InfoTemplate("<br/>", "<strong>${Name}</strong><br/>");
   trailITemp = new esri.InfoTemplate("<br/>", "<strong>${TRAIL_NAME}</strong><br/>Type: ${TRAIL_TYPE}<br/> Difficulty: ${DIFFICULTY}<br/> Paved: ${PAVED}<br/>");
   busITemp = new esri.InfoTemplate("<br/>", "<strong>" + this.toTitleCase("${TradingAsN}") + '</strong><br/>${NAICDesc}<br/><br/> <strong>Address: </strong>${Location}<br/><a href="http://maps.google.com/maps?daddr=${Location}, Danville, VA&hl=en" target="_blank">Directions</a>');
   var str = '<strong>Owner: </strong>${Owner 1}<br/><br/><strong>Address: </strong>${Address}<br/><br/><strong>Use Code: </strong>${Use Code}<br/><br/><a href="http://gis.danville-va.gov/ParcelViewer/?parcel_number=${Parcel Account ID}" target="_blank">View in Assessor Map</a>';
   parcITemp = new esri.InfoTemplate("<br/>", str);
}

function executeQueryTask() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/1", "Historic Spot", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, historics, ["ADDRESS", "NAME"], getRecCents);
}

function getRecCents() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/2", "Recreation Center", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, recCents, ["ADDRESS", "NAME"], getMuses);
}

function getMuses() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/3", "Museum", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, muses, ["ADDRESS", "NAME"], getGovs);
}

function getGovs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/4", "Government", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, govs, ["ADDRESS", "NAME"], getNonProfs);
}

function getNonProfs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/5", "Non-Profit", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, nonProfs, ["ADDRESS", "NAME"], getLibs);
}

function getGolfs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/6", "Golf", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, golfs, ["ADDRESS", "NAME"], getTrails);
}

function getLibs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/7", "Library", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, libs, ["ADDRESS", "NAME"], getPosts);
}

function getPosts() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/8", "Post Office", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, posts, ["ADDRESS", "NAME"], getAirs);
}

function getAirs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/9", "Airport", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, airs, ["ADDRESS", "NAME"], getBuses);
}

function getBuses() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/10", "Bus Stop", ["OBJECTID", "DESCRIPTION", "ADDRESS"], "1=1", true, busResults, ["ADDRESS"], getTrains);
}

function getTrains() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/11", "Train Station", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, trains, ["ADDRESS", "NAME"], getGraves);
}

function getGraves() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/12", "Cemetery", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, graves, ["ADDRESS", "NAME"], getFStats);
}

function getFStats() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/13", "Fire Station", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, fStats, ["ADDRESS", "NAME"], getPStats);
}

function getPStats() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/14", "Police Station", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, pStats, ["ADDRESS", "NAME"], getESchools);
}

function getPreSchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/16", "Preschool", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, preSchools, ["ADDRESS", "SCHOOL"], getPrivSchools);
}

function getESchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/17", "Elementary", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, eSchools, ["ADDRESS", "SCHOOL"], getMSchools);
}

function getMSchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/18", "Middle", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, mSchools, ["ADDRESS", "SCHOOL"], getHSchools);
}

function getHSchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/19", "High/Secondary", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, hSchools, ["ADDRESS", "SCHOOL"], getHighEds);
}

function getHighEds() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/20", "Higher Education", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, highEds, ["ADDRESS", "SCHOOL"], getSpecSchools);
}

function getSpecSchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/21", "Specialty", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, specSchools, ["ADDRESS", "SCHOOL"], getPreSchools);
}

function getPrivSchools() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/22", "Private", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", true, privSchools, ["ADDRESS", "SCHOOL"], getParks);
}

function getParks() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/23", "Park", ["OBJECTID", "Name"], "1=1", true, parks, ["Name"], getGolfs);
}

function getTrails() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/24", "Trails", ["OBJECTID", "TRAIL_NAME", "TRAIL_TYPE", "DIFFICULTY", "PAVED"], "1=1", true, trails, ["TRAIL_NAME"], getTrashes);
}

function getTrashes() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/25", "Trash Pickup", ["ZONE", "DAY"], "1=1", true, trashes, [], getBeauts);
}

function getBeauts() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/27", "Beauty/Barber", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, beauts, ["TradingAsN", "Location"], getRepairs);
}

function getRepairs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/28", "Repair", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, repairs, ["TradingAsN", "Location"], getMerches);
}

function getMerches() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/29", "Retail", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, merches, ["TradingAsN", "Location"], getShops);
}

function getShops() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/30", "Shopping Center", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", true, shops, ["ADDRESS", "NAME"], getMeds);
}

function getMeds() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/31", "Health Care", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, meds, ["TradingAsN", "Location"], getFoods);
}

function getFoods() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/32", "Restaurant", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, foods, ["TradingAsN", "Location"], getHotels);
}

function getHotels() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/33", "Room/Board", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, hotels, ["TradingAsN", "Location"], getRecs);
}

function getRecs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/34", "Recreation/Leisure", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, recs, ["TradingAsN", "Location"], getEdus);
}

function getEdus() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/35", "Education Services", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, eduServs, ["TradingAsN", "Location"], getBanks);
}

function getBanks() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/36", "Bank", ["OBJECTID", "NAME", "ADDRESS"], "1=1", true, banks, ["NAME", "ADDRESS"], getFins);
}

function getFins() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/37", "Financial Services", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, finances, ["TradingAsN", "Location"], getManufacts);
}

function getManufacts() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/38", "Manufacturing", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, manufacts, ["TradingAsN", "Location"], getContracts);
}

function getContracts() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/39", "Contractor", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, contracts, ["TradingAsN", "Location"], getOthers);
}

function getOthers() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/40", "Other Services", ["NAICDesc", "TradingAsN", "Location"], "1=1", true, others, ["TradingAsN", "Location"], getHDists);
}

function getHDists() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/42", "Historic Districts", ["DESCR", "DISTRICT"], "1=1", true, hists, [], getZones);
}

function getZones() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/44", "Zoning", ["ZONE", "CONDITION", "ORDINANCE"], "1=1", true, zones, [], getRental);
}

function getRental() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/45", "Rental Inspection Districts", ["NAME"], "1=1", true, rentals, [], getHubs);
}

function getHubs() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/46", "SBA Hub Zones", ["TRACT", "EnterpriseGIS_SDE_SBA_Hub_Z_3", "EnterpriseGIS_SDE_SBA_Hub_Z_4"], "1=1", true, hubs, [], getEZones);
}

function getEZones() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/47", "Enterprise Zones", ["CLASS", "SUM_AREA"], "1=1", true, eZones, [], getCBlocks1);
}

function getCBlocks1() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/48", "2010 Census Blocks", ["NAME10"], "BLOCKCE10 LIKE '10%'", true, cBlocks, [], getCBlocks2);
}

function getCBlocks2() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/48", "2010 Census Blocks", ["NAME10"], "BLOCKCE10 LIKE '20%'", true, cBlocks, [], getCBlocks3);
}

function getCBlocks3() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/48", "2010 Census Blocks", ["NAME10"], "BLOCKCE10 LIKE '30%'", true, cBlocks, [], getCBlocks4);
}

function getCBlocks4() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/48", "2010 Census Blocks", ["NAME10"], "BLOCKCE10 LIKE '40%'", true, cBlocks, [], finishLoad);
}

function finishLoad() {
   progress.hide();
   dojo.connect(map, "onPanEnd", newCenterHandler);
   dojo.connect(map, "onZoomEnd", zoomChange);
   dojo.connect(map, "onZoomEnd", newCenterHandler);
   dojo.connect(map, "resize", newCenterHandler);
   for (var i = 0, l = servChildren.length; i < l; i++) {
      dojo.connect(servChildren[i], "onClick", toggleCheckBox);
   }
   for (var i = 0, l = schoolChildren.length; i < l; i++) {
      dojo.connect(schoolChildren[i], "onClick", toggleCheckBox);
   }
   for (var i = 0, l = busChildren.length; i < l; i++) {
      dojo.connect(busChildren[i], "onClick", toggleCheckBox);
   }
   newCenterHandler();
}

function closeByPoint(fSetResults) {
   var dists = [];

   //Calculate the straight line distance between centerpoint and feature.
   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   //Convert distance to miles
   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   var returnObj = dists[0],
      smallest = dists[0].dist;
   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }

   uniqueID++;
   return returnObj;
}

function closeSetByPoint(fSetResults, num) {
   if (fSetResults != null && fSetResults != undefined) {
      var dists = [];
      var closest = [];

      //Calculate the straight line distance between centerpoint and feature.
      for (var i = 0, l = fSetResults.length; i < l; i++) {
         uniqueID++;
         dists.push({
            dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         });
      }

      //Convert distance to miles
      for (var i = 0, l = dists.length; i < l; i++) {
         dists[i].dist = (dists[i].dist / 5280).toFixed(2);
      }

      dists.sort(function (a, b) { return a.dist - b.dist });

      if (num == null || num == "") {
         return [dists[0]];
      }
      else {
         if (num > dists.length) {
            num = dists.length;
         }
         if (num < 0) {
            num = 1;
         }
         for (var i = 0; i < num; i++) {
            closest.push(dists[i]);
         }
      }

      uniqueID++;
      return closest;
   }
}

function closeSetByDist(fSetResults, rad) {
   var dists = [];
   var closest = [];

   //Calculate the straight line distance between centerpoint and feature.
   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   //Convert distance to miles
   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   dists.sort(function (a, b) { return a.dist - b.dist });

   if (rad == null || rad == "") {
      return [dists[0]];
   }
   else {
      for (var i = 0; i < dists.length; i++) {
         if ((+dists[i].dist) < (+rad)) {
            closest.push(dists[i]);
         }
      }
   }

   uniqueID++;
   return closest;
}

function closeByCenter(fSetResults) {
   var dists = [],
             smallest,
             returnObj = null;

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
         uniqueID++;
         return {
            dist: 0,
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         };
      }
   }

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }


   if (dists.length > 0) {
      returnObj = dists[0],
      smallest = dists[0].dist;
   }
   for (i = 0; i < dists.length; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }
   return returnObj;
}

function closeCenterByDist(fSetResults, rad) {
   var dists = [],
       returnObj = [];

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   dists.sort(function (a, b) { return a.dist - b.dist });

   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(rad)) {
         returnObj.push(dists[i]);
      }
   }
   return returnObj;
}

function closeCenterByNum(fSetResults, num) {
   if (fSetResults != null && fSetResults != undefined) {
      var dists = [],
       returnObj = [];

      for (var i = 0, l = fSetResults.length; i < l; i++) {
         uniqueID++;
         dists.push({
            dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         });
      }

      for (var i = 0, l = dists.length; i < l; i++) {
         dists[i].dist = (dists[i].dist / 5280).toFixed(2);
      }

      dists.sort(function (a, b) { return a.dist - b.dist });

      for (i = 0, l = dists.length; i < l; i++) {
         if (i < num) {
            returnObj.push(dists[i]);
         }
      }
      return returnObj;
   }
}

function closeByOutline(fSetResults) {
   if (fSetResults != null && fSetResults != undefined) {
      var dists = [],
             smallest,
             returnObj = null;

      for (var i = 0, l = fSetResults.length; i < l; i++) {
         if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
            uniqueID++;
            return {
               dist: 0,
               serv: fSetResults[i].attributes,
               features: fSetResults[i],
               uID: uniqueID
            };
         }
      }

      for (var i = 0, l = fSetResults.length; i < l; i++) {
         for (var k = 0, j = fSetResults[i].geometry.rings.length; k < j; k++) {
            for (var m = 0, n = fSetResults[i].geometry.rings[k].length; m < n; m++) {
               uniqueID++;
               dists.push({
                  dist: calcDist(new esri.geometry.Point(fSetResults[i].geometry.rings[k][m][0], fSetResults[i].geometry.rings[k][m][1], spatRef), map.extent.getCenter()),
                  serv: fSetResults[i].attributes,
                  features: fSetResults[i],
                  uID: uniqueID
               });
            }
         }
      }

      for (var i = 0, l = dists.length; i < l; i++) {
         dists[i].dist = (dists[i].dist / 5280).toFixed(2);
      }


      returnObj = dists[0],
      smallest = dists[0].dist;
      for (i = 0, l = dists.length; i < l; i++) {
         if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
            smallest = dists[i].dist;
            returnObj = dists[i];
         }
      }

      return returnObj;
   }
}

function closeByPolyline(fSetResults) {
   var dists = [],
       smallest,
       returnObj = null;

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      for (var j = 0, k = fSetResults[i].geometry.paths.length; j < k; j++) {
         uniqueID++;
         dists.push({
            dist: calcDist(new esri.geometry.Point(fSetResults[i].geometry.paths[j][0], fSetResults[i].geometry.paths[j][1], spatRef), map.extent.getCenter()),
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         });
      }
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   returnObj = dists[0],
         smallest = dists[0].dist;
   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }

   return returnObj;
}

function inFeature(fSetResults) {
   if (fSetResults != null && fSetResults != undefined) {
      for (var i = 0, l = fSetResults.length; i < l; i++) {
         if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
            uniqueID++;
            return true;
         }
      }
      uniqueID++;
      return false;
   }
}

function zoomChange(ext, zFactor, anch, level) {
   if (level > 7) {
      zoomed = true;
      searchMapLayer.clear();
      dynamicLayer.setVisibleLayers([1, 2, 7, 8, 9]);
   }
   else {
      zoomed = false;
      if (level >= 6) {
         dynamicLayer.setVisibleLayers([1, 7, 8, 9]);
      }
      if (level <= 5) {
         dynamicLayer.setVisibleLayers([7]);
      }
   }
}

function pushIdResults(results) {
   for (var i = 0, l = results.length; i < l; i++) {
      var graphic = results[i].feature;
      graphic.setInfoTemplate(parcITemp);
      graphic.setSymbol(parcSym);
      searchMapLayer.add(graphic);
   }
}

function doParcelIdentify(evt) {
   if (zoomed) {
      clickPoint = evt.mapPoint;
      idparams.mapExtent = map.extent;
      idparams.geometry = evt.mapPoint;
      identify.execute(idparams, pushResults);
   }
}

function pushResults(results) {
   for (var i = 0, l = results.length; i < l; i++) {
      var graphic = results[i].feature;
      graphic.setInfoTemplate(parcITemp);
      map.infoWindow.setFeatures([graphic]);
      map.infoWindow.show(clickPoint);
   }
}


function newCenterHandler(evt) {
   if (!wanted && !searched) {
      searchMapLayer.clear();
   }
   else {
      wanted = false;
      searched = false;
   }
   var graphic,
         cHistorics,
         cRecCents,
         cMuses,
         cGovs,
         cNonProfs,
         cLibs,
         cPosts,
         cAirs,
         cTrains,
         cGraves,
         cPStats,
         cGolfs,
         cFStats,
         cESchools,
         cMSchools,
         cHSchools,
         cHighEds,
         cSpecSchools,
         cPreSchools,
         cPrivSchools,
         cBus,
         cTrash,
         cParks,
         cTrails,
         cCBlock,
         cEZone,
         cFlood,
         cHist,
         cZone,
		 cRental,
         cHub,
         cBeauts,
         cRepairs,
         cMerches,
         cShops,
         cMeds,
         cFoods,
         cHotels,
         cManufacts,
         cContracts,
         cEdus,
         cBanks,
         cFins,
         cRecs,
         cOthers;

   if (maxFeatures.checked) {
      cHistorics = closeSetByPoint(historics, totalFeatures.value);
      cRecCents = closeSetByPoint(recCents, totalFeatures.value);
      cMuses = closeSetByPoint(muses, totalFeatures.value);
      cGovs = closeSetByPoint(govs, totalFeatures.value);
      cNonProfs = closeSetByPoint(nonProfs, totalFeatures.value);
      cLibs = closeSetByPoint(libs, totalFeatures.value);
      cPosts = closeSetByPoint(posts, totalFeatures.value);
      cAirs = closeSetByPoint(airs, totalFeatures.value);
      cTrains = closeSetByPoint(trains, totalFeatures.value);
      cGraves = closeSetByPoint(graves, totalFeatures.value);
      cFStats = closeSetByPoint(fStats, totalFeatures.value);
      cPStats = closeSetByPoint(pStats, totalFeatures.value);
      cESchools = closeSetByPoint(eSchools, totalFeatures.value);
      cMSchools = closeSetByPoint(mSchools, totalFeatures.value);
      cHSchools = closeSetByPoint(hSchools, totalFeatures.value);
      cHighEds = closeSetByPoint(highEds, totalFeatures.value);
      cSpecSchools = closeSetByPoint(specSchools, totalFeatures.value);
      cPreSchools = closeSetByPoint(preSchools, totalFeatures.value);
      cPrivSchools = closeSetByPoint(privSchools, totalFeatures.value);
      cParks = closeCenterByNum(parks, totalFeatures.value);
      cGolfs = closeSetByPoint(golfs, totalFeatures.value);
      cTrails = closeCenterByNum(trails, totalFeatures.value);
      cBus = closeSetByPoint(busResults, totalFeatures.value);
      cBeauts = closeSetByPoint(beauts, totalFeatures.value);
      cRepairs = closeSetByPoint(repairs, totalFeatures.value);
      cMerches = closeSetByPoint(merches, totalFeatures.value);
      cShops = closeSetByPoint(shops, totalFeatures.value);
      cMeds = closeSetByPoint(meds, totalFeatures.value);
      cFoods = closeSetByPoint(foods, totalFeatures.value);
      cHotels = closeSetByPoint(hotels, totalFeatures.value);
      cRecs = closeSetByPoint(recs, totalFeatures.value);
      cEdus = closeSetByPoint(eduServs, totalFeatures.value);
      cBanks = closeSetByPoint(banks, totalFeatures.value);
      cFins = closeSetByPoint(finances, totalFeatures.value);
      cManufacts = closeSetByPoint(manufacts, totalFeatures.value);
      cContracts = closeSetByPoint(contracts, totalFeatures.value);
      cOthers = closeSetByPoint(others, totalFeatures.value);
	  cRental = closeSetByPoint(rentals, totalFeatures.value);
   }
   else {
      cHistorics = closeSetByDist(historics, radius.value);
      cRecCents = closeSetByDist(recCents, radius.value);
      cMuses = closeSetByDist(muses, radius.value);
      cGovs = closeSetByDist(govs, radius.value);
      cNonProfs = closeSetByDist(nonProfs, radius.value);
      cLibs = closeSetByDist(libs, radius.value);
      cPosts = closeSetByDist(posts, radius.value);
      cAirs = closeSetByDist(airs, radius.value);
      cTrains = closeSetByDist(trains, radius.value);
      cGraves = closeSetByDist(graves, radius.value);
      cFStats = closeSetByDist(fStats, radius.value);
      cPStats = closeSetByDist(pStats, radius.value);
      cESchools = closeSetByDist(eSchools, radius.value);
      cMSchools = closeSetByDist(mSchools, radius.value);
      cHSchools = closeSetByDist(hSchools, radius.value);
      cHighEds = closeSetByDist(highEds, radius.value);
      cSpecSchools = closeSetByDist(specSchools, radius.value);
      cPreSchools = closeSetByDist(preSchools, radius.value);
      cPrivSchools = closeSetByDist(privSchools, radius.value);
      cParks = closeCenterByDist(parks, radius.value);
      cGolfs = closeSetByDist(golfs, radius.value);
      cTrails = closeCenterByDist(trails, radius.value);
      cBus = closeSetByDist(busResults, radius.value);
      cBeauts = closeSetByDist(beauts, radius.value);
      cRepairs = closeSetByDist(repairs, radius.value);
      cMerches = closeSetByDist(merches, radius.value);
      cShops = closeSetByDist(shops, radius.value);
      cMeds = closeSetByDist(meds, radius.value);
      cFoods = closeSetByDist(foods, radius.value);
      cHotels = closeSetByDist(hotels, radius.value);
      cRecs = closeSetByDist(recs, radius.value);
      cEdus = closeSetByDist(eduServs, radius.value);
      cBanks = closeSetByDist(banks, radius.value);
      cFins = closeSetByDist(finances, radius.value);
      cManufacts = closeSetByDist(manufacts, radius.value);
      cContracts = closeSetByDist(contracts, radius.value);
      cOthers = closeSetByDist(others, radius.value);
	  cRental = closeSetByDist(rentals, radius.value);
   }

   if (!zoomed) {
      if (park.checked && !park.disabled && cParks != null && cParks != undefined) {
         if (cParks.length > 0) {
            wipeParkRowIn.play();

            dojo.attr(parkRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Park",
               name: "Name",
               address: null,
               googleAddress: "Name"
            }

            for (var i = 0; i < cParks.length; i++) {
               params.features.push(cParks[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cParks.length; i++) {
               var graphic = cParks[i].features;
               graphic.setSymbol(pSym);
               graphic.setInfoTemplate(pITemp);
               if (cParks[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(pITemp.title, pITemp.content + '<br/><a href="' + cParks[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(parkRow, 'innerHTML', str);
         }
         else {
            wipeParkRowOut.play();
            dojo.attr(parkRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(parkRow, 'innerHTML', '');
         wipeParkRowOut.play();
         dojo.attr(parkRow, 'class', 'notDisplayed');
      }
   }

   if (other.checked && !other.disabled && cOthers != null && cOthers != undefined) {
      if (cOthers.length > 0) {
         wipeOtherRowIn.play();

         dojo.attr(otherRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Other Services",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cOthers.length; i++) {
            params.features.push(cOthers[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cOthers.length; i++) {
            graphic = cOthers[i].features;
            graphic.setSymbol(otherSym);
            graphic.setInfoTemplate(busITemp);
            if (cOthers[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cOthers[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(otherRow, 'innerHTML', str);
      }
      else {
         wipeOtherRowOut.play();
         dojo.attr(otherRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(otherRow, 'innerHTML', '');
      wipeOtherRowOut.play();
      dojo.attr(otherRow, 'class', 'notDisplayed');
   }

   if (contract.checked && !contract.disabled && cContracts != null && cContracts != undefined) {
      if (cContracts.length > 0) {
         wipeContractRowIn.play();

         dojo.attr(contractRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Contractor",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cContracts.length; i++) {
            params.features.push(cContracts[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cContracts.length; i++) {
            graphic = cContracts[i].features;
            graphic.setSymbol(contractSym);
            graphic.setInfoTemplate(busITemp);
            if (cContracts[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cContracts[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(contractRow, 'innerHTML', str);
      }
      else {
         wipeContractRowOut.play();
         dojo.attr(contractRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(contractRow, 'innerHTML', '');
      wipeContractRowOut.play();
      dojo.attr(contractRow, 'class', 'notDisplayed');
   }

   if (manufact.checked && !manufact.disabled && cManufacts != null && cManufacts != undefined) {
      if (cManufacts.length > 0) {
         wipeManufactRowIn.play();

         dojo.attr(manufactRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Manufacturing",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cManufacts.length; i++) {
            params.features.push(cManufacts[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cManufacts.length; i++) {
            graphic = cManufacts[i].features;
            graphic.setSymbol(manufactSym);
            graphic.setInfoTemplate(busITemp);
            if (cManufacts[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cManufacts[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(manufactRow, 'innerHTML', str);
      }
      else {
         wipeManufactRowOut.play();
         dojo.attr(manufactRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(manufactRow, 'innerHTML', '');
      wipeManufactRowOut.play();
      dojo.attr(manufactRow, 'class', 'notDisplayed');
   }

   if (finServ.checked && !finServ.disabled && cFins != null && cFins != undefined) {
      if (cFins.length > 0) {
         wipeFinServRowIn.play();

         dojo.attr(finServRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Financial Services",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cFins.length; i++) {
            params.features.push(cFins[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cFins.length; i++) {
            graphic = cFins[i].features;
            graphic.setSymbol(finSym);
            graphic.setInfoTemplate(busITemp);
            if (cFins[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cFins[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(finServRow, 'innerHTML', str);
      }
      else {
         wipeFinServRowOut.play();
         dojo.attr(finServRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(finServRow, 'innerHTML', '');
      wipeFinServRowOut.play();
      dojo.attr(finServRow, 'class', 'notDisplayed');
   }

   if (eduServ.checked && !eduServ.disabled && cEdus != null && cEdus != undefined) {
      if (cEdus.length > 0) {
         wipeEduServRowIn.play();

         dojo.attr(eduServRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Educational Services",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cEdus.length; i++) {
            params.features.push(cEdus[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cEdus.length; i++) {
            graphic = cEdus[i].features;
            graphic.setSymbol(eduSSym);
            graphic.setInfoTemplate(busITemp);
            if (cEdus[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cEdus[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(eduServRow, 'innerHTML', str);
      }
      else {
         wipeEduServRowOut.play();
         dojo.attr(eduServRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(eduServRow, 'innerHTML', '');
      wipeEduServRowOut.play();
      dojo.attr(eduServRow, 'class', 'notDisplayed');
   }

   if (rec.checked && !rec.disabled && cRecs != null && cRecs != undefined) {
      if (cRecs.length > 0) {
         wipeRecRowIn.play();

         dojo.attr(recRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Recreation/Leisure",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cRecs.length; i++) {
            params.features.push(cRecs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cRecs.length; i++) {
            graphic = cRecs[i].features;
            graphic.setSymbol(recSym);
            graphic.setInfoTemplate(busITemp);
            if (cRecs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cRecs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(recRow, 'innerHTML', str);
      }
      else {
         wipeRecRowOut.play();
         dojo.attr(recRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(recRow, 'innerHTML', '');
      wipeRecRowOut.play();
      dojo.attr(recRow, 'class', 'notDisplayed');
   }

   if (med.checked && !med.disabled && cMeds != null && cMeds != undefined) {
      if (cMeds.length > 0) {
         wipeMedRowIn.play();

         dojo.attr(medRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Health Care",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cMeds.length; i++) {
            params.features.push(cMeds[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cMeds.length; i++) {
            graphic = cMeds[i].features;
            graphic.setSymbol(medSym);
            graphic.setInfoTemplate(busITemp);
            if (cMeds[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cMeds[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(medRow, 'innerHTML', str);
      }
      else {
         wipeMedRowOut.play();
         dojo.attr(medRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(medRow, 'innerHTML', '');
      wipeMedRowOut.play();
      dojo.attr(medRow, 'class', 'notDisplayed');
   }

   if (hotel.checked && !hotel.disabled && cHotels != null && cHotels != undefined) {
      if (cHotels.length > 0) {
         wipeHotelRowIn.play();

         dojo.attr(hotelRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Room/Board",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cHotels.length; i++) {
            params.features.push(cHotels[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cHotels.length; i++) {
            graphic = cHotels[i].features;
            graphic.setSymbol(hotelSym);
            graphic.setInfoTemplate(busITemp);
            if (cHotels[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cHotels[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(hotelRow, 'innerHTML', str);
      }
      else {
         wipeHotelRowOut.play();
         dojo.attr(hotelRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(hotelRow, 'innerHTML', '');
      wipeHotelRowOut.play();
      dojo.attr(hotelRow, 'class', 'notDisplayed');
   }

   if (food.checked && !food.disabled && cFoods != null && cFoods != undefined) {
      if (cFoods.length > 0) {
         wipeFoodRowIn.play();

         dojo.attr(foodRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Restaurant",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cFoods.length; i++) {
            params.features.push(cFoods[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cFoods.length; i++) {
            graphic = cFoods[i].features;
            graphic.setSymbol(foodSym);
            graphic.setInfoTemplate(busITemp);
            if (cFoods[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cFoods[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(foodRow, 'innerHTML', str);
      }
      else {
         wipeFoodRowOut.play();
         dojo.attr(foodRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(foodRow, 'innerHTML', '');
      wipeFoodRowOut.play();
      dojo.attr(foodRow, 'class', 'notDisplayed');
   }

   if (historic.checked && !historic.disabled && cHistorics != null && cHistorics != undefined) {
      if (cHistorics.length > 0) {

         wipeHistoricRowIn.play();

         dojo.attr(historicRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Historic Site",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }
         for (var i = 0; i < cHistorics.length; i++) {
            params.features.push(cHistorics[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cHistorics.length; i++) {
            var graphic = cHistorics[i].features;
            graphic.setSymbol(historSym);
            graphic.setInfoTemplate(servITemp);
            if (cHistorics[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cHistorics[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr('historicRow', 'innerHTML', str);
      }
      else {
         wipeHistoricRowOut.play();
         dojo.attr(historicRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(historicRow, 'innerHTML', '');
      wipeHistoricRowOut.play();
      dojo.attr(historicRow, 'class', 'notDisplayed');
   }

   if (recCent.checked && !recCent.disabled && cRecCents != null && cRecCents != undefined) {
      if (cRecCents.length > 0) {
         wipeRecCentRowIn.play();

         dojo.attr(recCentRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Recreation Center",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cRecCents.length; i++) {
            params.features.push(cRecCents[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cRecCents.length; i++) {
            var graphic = cRecCents[i].features;
            graphic.setSymbol(recCentSym);
            graphic.setInfoTemplate(servITemp);
            if (cRecCents[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cRecCents[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(recCentRow, 'innerHTML', str);
      }

      else {
         wipeRecCentRowOut.play();
         dojo.attr(recCentRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(recCentRow, 'innerHTML', '');
      wipeRecCentRowOut.play();
      dojo.attr(recCentRow, 'class', 'notDisplayed');
   }

   if (muse.checked && !muse.disabled && cMuses != null && cMuses != undefined) {
      if (cMuses.length > 0) {
         wipeMuseRowIn.play();

         dojo.attr(museRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Museum",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }
         for (var i = 0; i < cMuses.length; i++) {
            params.features.push(cMuses[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cMuses.length; i++) {
            var graphic = cMuses[i].features;
            graphic.setSymbol(museSym);
            graphic.setInfoTemplate(servITemp);
            if (cMuses[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cMuses[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(museRow, 'innerHTML', str);
      }

      else {
         wipeMuseRowOut.play();

         dojo.attr(museRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(museRow, 'innerHTML', '');
      wipeMuseRowOut.play();

      dojo.attr(museRow, 'class', 'notDisplayed');
   }

   if (gov.checked && !gov.disabled && cGovs != null && cGovs != undefined) {
      if (cGovs.length > 0) {
         wipeGovRowIn.play();

         dojo.attr(govRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Government",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }
         for (var i = 0; i < cGovs.length; i++) {
            params.features.push(cGovs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cGovs.length; i++) {
            var graphic = cGovs[i].features;
            graphic.setSymbol(govSym);
            graphic.attributes.NAME = toTitleCase(graphic.attributes.NAME);
            graphic.setInfoTemplate(servITemp);
            if (cGovs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cGovs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(govRow, 'innerHTML', str);
      }

      else {
         wipeGovRowOut.play();

         dojo.attr(govRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(govRow, 'innerHTML', '');
      wipeGovRowOut.play();

      dojo.attr(govRow, 'class', 'notDisplayed');
   }

   if (nonProf.checked && !nonProf.disabled && cNonProfs != null && cNonProfs != undefined) {
      if (cNonProfs.length > 0) {
         wipeNonProfRowIn.play();

         dojo.attr(nonProfRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Non-Profit",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }
         for (var i = 0; i < cNonProfs.length; i++) {
            params.features.push(cNonProfs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cNonProfs.length; i++) {
            var graphic = cNonProfs[i].features;
            graphic.setSymbol(nonProfSym);
            graphic.setInfoTemplate(servITemp);
            if (cNonProfs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cNonProfs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(nonProfRow, 'innerHTML', str);
      }

      else {
         wipeNonProfRowOut.play();

         dojo.attr(nonProfRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(nonProfRow, 'innerHTML', '');
      wipeNonProfRowOut.play();

      dojo.attr(nonProfRow, 'class', 'notDisplayed');
   }

   if (lib.checked && !lib.disabled && cLibs != null && cLibs != undefined) {
      if (cLibs.length > 0) {
         wipeLibRowIn.play();

         dojo.attr(libRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Library",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }
         for (var i = 0; i < cLibs.length; i++) {
            params.features.push(cLibs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cLibs.length; i++) {
            var graphic = cLibs[i].features;
            graphic.setSymbol(libSym);
            graphic.setInfoTemplate(servITemp);
            if (cLibs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cLibs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(libRow, 'innerHTML', str);
      }

      else {
         wipeLibRowOut.play();
         dojo.attr(libRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(libRow, 'innerHTML', '');
      wipeLibRowOut.play();
      dojo.attr(libRow, 'class', 'notDisplayed');
   }

   if (post.checked && !post.disabled && cPosts != null && cPosts != undefined) {
      if (cPosts.length > 0) {
         wipePostRowIn.play();

         dojo.attr(postRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Post Office",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cPosts.length; i++) {
            params.features.push(cPosts[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cPosts.length; i++) {
            var graphic = cPosts[i].features;
            graphic.setSymbol(postSym);
            graphic.setInfoTemplate(servITemp);
            if (cPosts[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cPosts[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(postRow, 'innerHTML', str);
      }

      else {
         wipePostRowOut.play();

         dojo.attr(postRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(postRow, 'innerHTML', '');
      wipePostRowOut.play();
      dojo.attr(postRow, 'class', 'notDisplayed');
   }


   if (air.checked && !air.disabled && cAirs != null && cAirs != undefined) {
      if (cAirs.length > 0) {
         wipeAirRowIn.play();

         dojo.attr(airRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Airport",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cAirs.length; i++) {
            params.features.push(cAirs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cAirs.length; i++) {
            var graphic = cAirs[i].features;
            graphic.setSymbol(airSym);
            graphic.setInfoTemplate(servITemp);
            if (cAirs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cAirs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(airRow, 'innerHTML', str);
      }

      else {
         wipeAirRowOut.play();
         dojo.attr(airRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(airRow, 'innerHTML', '');
      wipeAirRowOut.play();
      dojo.attr(airRow, 'class', 'notDisplayed');
   }

   if (bus.checked && !bus.disabled && cBus != null && cBus != undefined) {
      if (cBus.length > 0) {
         if (cBus.length > 0) {
            wipeBusRowIn.play();

            dojo.attr(busRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Bus Stop",
               name: "DESCRIPTION",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cBus.length; i++) {
               params.features.push(cBus[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cBus.length; i++) {
               var graphic = cBus[i].features;
               graphic.setSymbol(bSSym);
               graphic.setInfoTemplate(bSITemp);
               if (cBus[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(bsITemp.title, bsITemp.content + '<br/><a href="' + cBus[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(busRow, 'innerHTML', str);
         }
      }
      else {
         wipeBusRowOut.play();
         dojo.attr(busRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(busRow, 'innerHTML', '');
      wipeBusRowOut.play();
      dojo.attr(busRow, 'class', 'notDisplayed');
   }

   if (train.checked && !train.disabled && cTrains != null && cTrains != undefined) {
      if (cTrains.length > 0) {
         wipeTrainRowIn.play();

         dojo.attr(trainRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Train Station",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cTrains.length; i++) {
            params.features.push(cTrains[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cTrains.length; i++) {
            var graphic = cTrains[i].features;
            graphic.setSymbol(trainSym);
            graphic.setInfoTemplate(servITemp);
            if (cTrains[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cTrains[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(trainRow, 'innerHTML', str);
      }
      else {
         wipeTrainRowOut.play();
         dojo.attr(trainRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(trainRow, 'innerHTML', '');
      wipeTrainRowOut.play();
      dojo.attr(trainRow, 'class', 'notDisplayed');
   }

   if (grave.checked && !grave.disabled && cGraves != null && cGraves != undefined) {
      if (cGraves.length > 0) {
         wipeGraveRowIn.play();

         dojo.attr(graveRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Cemetery",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cGraves.length; i++) {
            params.features.push(cGraves[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cGraves.length; i++) {
            var graphic = cGraves[i].features;
            graphic.setSymbol(graveSym);
            graphic.setInfoTemplate(servITemp);
            if (cGraves[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cGraves[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(graveRow, 'innerHTML', str);
      }
      else {
         wipeGraveRowOut.play();
         dojo.attr(graveRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(graveRow, 'innerHTML', '');
      wipeGraveRowOut.play();
      dojo.attr(graveRow, 'class', 'notDisplayed');
   }

   if (fStat.checked && !fStat.disabled && cFStats != null && cFStats != undefined) {
      if (cFStats.length > 0) {
         wipeFStatRowIn.play();

         dojo.attr(fStatRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Fire Station",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cFStats.length; i++) {
            params.features.push(cFStats[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cFStats.length; i++) {
            var graphic = cFStats[i].features;
            graphic.setSymbol(fSSym);
            graphic.setInfoTemplate(servITemp);
            if (cFStats[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cFStats[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(fStatRow, 'innerHTML', str);
      }
      else {
         wipeFStatRowOut.play();
         dojo.attr(fStatRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(fStatRow, 'innerHTML', '');
      wipeFStatRowOut.play();
      dojo.attr(fStatRow, 'class', 'notDisplayed');
   }

   if (pStat.checked && !pStat.disabled && cPStats != null && cPStats != undefined) {
      if (cPStats.length > 0) {
         wipePStatRowIn.play();

         dojo.attr(pStatRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Police Station",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cPStats.length; i++) {
            params.features.push(cPStats[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cPStats.length; i++) {
            var graphic = cPStats[i].features;
            graphic.setSymbol(pSSym);
            graphic.setInfoTemplate(servITemp);
            if (cPStats[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cPStats[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(pStatRow, 'innerHTML', str);
      }
      else {
         wipePStatRowOut.play();
         dojo.attr(pStatRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(pStatRow, 'innerHTML', '');
      wipePStatRowOut.play();
      dojo.attr(pStatRow, 'class', 'notDisplayed');
   }

   if (edu.checked && !edu.disabled) {
      if (eSchool.checked && !eSchool.disabled && cESchools != null && cESchools != undefined) {
         if (cESchools.length > 0) {
            wipeESRowIn.play();

            dojo.attr(eSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Elementary School",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cESchools.length; i++) {
               params.features.push(cESchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cESchools.length; i++) {
               var graphic = cESchools[i].features;
               graphic.setSymbol(eSSym);
               graphic.setInfoTemplate(schITemp);
               if (cESchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cESchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(eSRow, 'innerHTML', str);
         }
         else {
            wipeESRowOut.play();
            dojo.attr(eSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(eSRow, 'innerHTML', '');
         wipeESRowOut.play();
         dojo.attr(eSRow, 'class', 'notDisplayed');
      }

      if (mSchool.checked && !mSchool.disabled && cMSchools != null && cMSchools != undefined) {
         if (cMSchools.length > 0) {
            wipeMSRowIn.play();

            dojo.attr(mSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Middle School",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cMSchools.length; i++) {
               params.features.push(cMSchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cMSchools.length; i++) {
               var graphic = cMSchools[i].features;
               graphic.setSymbol(mSSym);
               graphic.setInfoTemplate(schITemp);
               if (cMSchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cMSchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(mSRow, 'innerHTML', str);
         }
         else {
            wipeMSRowOut.play();
            dojo.attr(mSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(mSRow, 'innerHTML', '');
         wipeMSRowOut.play();
         dojo.attr(mSRow, 'class', 'notDisplayed');
      }

      if (hSchool.checked && !hSchool.disabled && cHSchools != null && cHSchools != undefined) {
         if (cHSchools.length > 0) {
            wipeHSRowIn.play();

            dojo.attr(hSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "High School",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cHSchools.length; i++) {
               params.features.push(cHSchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cHSchools.length; i++) {
               var graphic = cHSchools[i].features;
               graphic.setSymbol(hSSym);
               graphic.setInfoTemplate(schITemp);
               if (cHSchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cHSchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(hSRow, 'innerHTML', str);
         }
         else {
            wipeHSRowOut.play();
            dojo.attr(hSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(hSRow, 'innerHTML', '');
         wipeHSRowOut.play();
         dojo.attr(hSRow, 'class', 'notDisplayed');
      }

      if (highEd.checked && !highEd.disabled && cHighEds != null && cHighEds != undefined) {
         if (cHighEds.length > 0) {
            wipeHighEdRowIn.play();

            dojo.attr(highEdRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Higher Education",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cHighEds.length; i++) {
               params.features.push(cHighEds[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cHighEds.length; i++) {
               var graphic = cHighEds[i].features;
               graphic.setSymbol(highEdSym);
               graphic.setInfoTemplate(schITemp);
               if (cHighEds[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cHighEds[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(highEdRow, 'innerHTML', str);
         }
         else {
            wipeHighEdRowOut.play();
            dojo.attr(highEdRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(highEdRow, 'innerHTML', '');
         wipeHighEdRowOut.play();
         dojo.attr(highEdRow, 'class', 'notDisplayed');
      }

      if (specSchool.checked && !specSchool.disabled && cSpecSchools != null && cSpecSchools != undefined) {
         if (cSpecSchools.length > 0) {
            wipeSpecSRowIn.play();

            dojo.attr(specSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Specialty School",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cSpecSchools.length; i++) {
               params.features.push(cSpecSchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cSpecSchools.length; i++) {
               var graphic = cSpecSchools[i].features;
               graphic.setSymbol(specSSym);
               graphic.setInfoTemplate(schITemp);
               if (cSpecSchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cSpecSchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(specSRow, 'innerHTML', str);
         }
         else {
            wipeSpecSRowOut.play();
            dojo.attr(specSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(specSRow, 'innerHTML', '');
         wipeSpecSRowOut.play();
         dojo.attr(specSRow, 'class', 'notDisplayed');
      }

      if (preSchool.checked && !preSchool.disabled && cPreSchools != null && cPreSchools != undefined) {
         if (cPreSchools.length > 0) {
            wipePreSRowIn.play();

            dojo.attr(preSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Preschool",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cPreSchools.length; i++) {
               params.features.push(cPreSchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cPreSchools.length; i++) {
               var graphic = cPreSchools[i].features;
               graphic.setSymbol(preSSym);
               graphic.setInfoTemplate(schITemp);
               if (cPreSchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cPreSchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(preSRow, 'innerHTML', str);
         }
         else {
            wipePreSRowOut.play();
            dojo.attr(preSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(preSRow, 'innerHTML', '');
         wipePreSRowOut.play();
         dojo.attr(preSRow, 'class', 'notDisplayed');
      }

      if (privSchool.checked && !privSchool.disabled && cPrivSchools != null && cPrivSchools != undefined) {
         if (cPrivSchools.length > 0) {
            wipePrivSRowIn.play();

            dojo.attr(privSRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Private School",
               name: "SCHOOL",
               address: "ADDRESS",
               googleAddress: "ADDRESS"
            }

            for (var i = 0; i < cPrivSchools.length; i++) {
               params.features.push(cPrivSchools[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cPrivSchools.length; i++) {
               var graphic = cPrivSchools[i].features;
               graphic.setSymbol(privSSym);
               graphic.setInfoTemplate(schITemp);
               if (cPrivSchools[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(schITemp.title, schITemp.content + '<br/><a href="' + cPrivSchools[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(privSRow, 'innerHTML', str);
         }
         else {
            wipePrivSRowOut.play();
            dojo.attr(privSRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(privSRow, 'innerHTML', '');
         wipePrivSRowOut.play();
         dojo.attr(privSRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(eSRow, 'innerHTML', '');
      wipeESRowOut.play();
      dojo.attr(eSRow, 'class', 'notDisplayed');

      dojo.attr(mSRow, 'innerHTML', '');
      wipeMSRowOut.play();
      dojo.attr(mSRow, 'class', 'notDisplayed');

      dojo.attr(hSRow, 'innerHTML', '');
      wipeHSRowOut.play();
      dojo.attr(hSRow, 'class', 'notDisplayed');

      dojo.attr(highEdRow, 'innerHTML', '');
      wipeHighEdRowOut.play();
      dojo.attr(highEdRow, 'class', 'notDisplayed');

      dojo.attr(specSRow, 'innerHTML', '');
      wipeSpecSRowOut.play();
      dojo.attr(specSRow, 'class', 'notDisplayed');

      dojo.attr(preSRow, 'innerHTML', '');
      wipePreSRowOut.play();
      dojo.attr(preSRow, 'class', 'notDisplayed');

      dojo.attr(privSRow, 'innerHTML', '');
      wipePrivSRowOut.play();
      dojo.attr(privSRow, 'class', 'notDisplayed');
   }

   if (golf.checked && !golf.disabled && cGolfs != null && cGolfs != undefined) {
      if (cGolfs.length > 0) {
         wipeGolfRowIn.play();

         dojo.attr(golfRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Golf Course",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cGolfs.length; i++) {
            params.features.push(cGolfs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cGolfs.length; i++) {
            var graphic = cGolfs[i].features;
            graphic.setSymbol(golfSym);
            graphic.setInfoTemplate(servITemp);
            if (cGolfs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cGolfs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(golfRow, 'innerHTML', str);
      }
      else {
         wipeGolfRowOut.play();
         dojo.attr(golfRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(golfRow, 'innerHTML', '');
      wipeGolfRowOut.play();
      dojo.attr(golfRow, 'class', 'notDisplayed');
   }

   if (trail.checked && !trail.disabled && cTrails != null && cTrails != undefined) {
      if (cTrails.length > 0) {
         wipeTrailRowIn.play();

         dojo.attr(trailRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Trail",
            name: "TRAIL_NAME",
            address: null,
            googleAddress: "TRAIL_NAME"
         }

         for (var i = 0; i < cTrails.length; i++) {
            params.features.push(cTrails[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cTrails.length; i++) {
            graphic = cTrails[i].features;
            graphic.setSymbol(mTrailSym);
            if (cTrails[i].serv["TRAIL_TYPE"] == "Bike Off Road") {
               graphic.setSymbol(bTrailSym);
            }
            if (cTrails[i].serv["TRAIL_TYPE"] == "Hiking") {
               graphic.setSymbol(hTrailSym);
            }
            graphic.setInfoTemplate(trailITemp);
            if (cTrails[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(trailITemp.title, trailITemp.content + '<br/><a href="' + cTrails[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(trailRow, 'innerHTML', str);
      }
      else {
         wipeTrailRowOut.play();
         dojo.attr(trailRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(trailRow, 'innerHTML', '');
      wipeTrailRowOut.play();
      dojo.attr(trailRow, 'class', 'notDisplayed');
   }

   if (beauty.checked && !beauty.disabled && cBeauts != null && cBeauts != undefined) {
      if (cBeauts.length > 0) {
         wipeBeautyRowIn.play();

         dojo.attr(beautyRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Beauty/Barber",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cBeauts.length; i++) {
            params.features.push(cBeauts[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cBeauts.length; i++) {
            graphic = cBeauts[i].features;
            graphic.setSymbol(beautySym);
            graphic.setInfoTemplate(busITemp);
            if (cBeauts[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cBeauts[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(beautyRow, 'innerHTML', str);
      }
      else {
         wipeBeautyRowOut.play();
         dojo.attr(beautyRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(beautyRow, 'innerHTML', '');
      wipeBeautyRowOut.play();
      dojo.attr(beautyRow, 'class', 'notDisplayed');
   }

   if (repair.checked && !repair.disabled && cRepairs != null && cRepairs != undefined) {
      if (cRepairs.length > 0) {
         wipeRepairRowIn.play();

         dojo.attr(repairRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Repair",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cRepairs.length; i++) {
            params.features.push(cRepairs[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cRepairs.length; i++) {
            graphic = cRepairs[i].features;
            graphic.setSymbol(repairSym);
            graphic.setInfoTemplate(busITemp);
            if (cRepairs[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cRepairs[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(repairRow, 'innerHTML', str);
      }
      else {
         wipeRepairRowOut.play();
         dojo.attr(repairRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(repairRow, 'innerHTML', '');
      wipeRepairRowOut.play();
      dojo.attr(repairRow, 'class', 'notDisplayed');
   }

   if (retail.checked && !retail.disabled && cMerches != null && cMerches != undefined) {
      if (cMerches.length > 0) {
         wipeRetailRowIn.play();

         dojo.attr(retailRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Retail",
            name: "TradingAsN",
            address: "Location",
            googleAddress: "Location"
         }

         for (var i = 0; i < cMerches.length; i++) {
            params.features.push(cMerches[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cMerches.length; i++) {
            graphic = cMerches[i].features;
            graphic.setSymbol(retailSym);
            graphic.setInfoTemplate(busITemp);
            if (cMerches[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(busITemp.title, busITemp.content + '<br/><a href="' + cMerches[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(retailRow, 'innerHTML', str);
      }
      else {
         wipeRetailRowOut.play();
         dojo.attr(retailRow, 'class', 'notDisplayed');
      }
   }
   else {
      dojo.attr(retailRow, 'innerHTML', '');
      wipeRetailRowOut.play();
      dojo.attr(retailRow, 'class', 'notDisplayed');
   }

   if (shop.checked && !shop.disabled && cShops != null && cShops != undefined) {
      if (cShops.length > 0) {
         wipeShopRowIn.play();

         dojo.attr(shopRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Shopping Center",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cShops.length; i++) {
            params.features.push(cShops[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cShops.length; i++) {
            var graphic = cShops[i].features;
            graphic.setSymbol(shopSym);
            graphic.attributes.NAME = toTitleCase(graphic.attributes.NAME);
            graphic.setInfoTemplate(servITemp);
            if (cShops[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cShops[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(shopRow, 'innerHTML', str);
      }
      else {
         wipeShopRowOut.play();
         dojo.attr(shopRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(shopRow, 'innerHTML', '');
      wipeShopRowOut.play();
      dojo.attr(shopRow, 'class', 'notDisplayed');
   }

   if (bank.checked && !bank.disabled && cBanks != null && cBanks != undefined) {
      if (cBanks.length > 0) {
         wipeBankRowIn.play();

         dojo.attr(bankRow, 'class', 'isDisplayed');

         var params = {
            features: [],
            type: "Bank",
            name: "NAME",
            address: "ADDRESS",
            googleAddress: "ADDRESS"
         }

         for (var i = 0; i < cBanks.length; i++) {
            params.features.push(cBanks[i]);
         }

         str = null;
         str = buildTableRow(params);

         for (var i = 0; i < cBanks.length; i++) {
            var graphic = cBanks[i].features;
            graphic.setSymbol(bankSym);
            graphic.attributes.NAME = toTitleCase(graphic.attributes.NAME);
            graphic.setInfoTemplate(servITemp);
            if (cBanks[i].serv.URL != undefined) {
               var tempITemp = new esri.InfoTemplate(servITemp.title, servITemp.content + '<br/><a href="' + cBanks[i].serv.URL + '">website</a>');
               graphic.setInfoTemplate(tempITemp);
            }
            searchMapLayer.add(graphic);
         }
         dojo.attr(bankRow, 'innerHTML', str);
      }
      else {
         wipeBankRowOut.play();
         dojo.attr(bankRow, 'class', 'notDisplayed');
      }
   }

   else {
      dojo.attr(bankRow, 'innerHTML', '');
      wipeBankRowOut.play();
      dojo.attr(bankRow, 'class', 'notDisplayed');
   }

   if (!zoomed) {
      if (rental.checked && !rental.disabled && cRental != null && cRental != undefined) {
         if (cRental.length > 0) {
            wipeRentalRowIn.play();

            dojo.attr(rentalRow, 'class', 'isDisplayed');

            var params = {
               features: [],
               type: "Rental",
               name: "NAME",
               address: null,
               googleAddress: null
            }

            for (var i = 0; i < cRental.length; i++) {
               params.features.push(cRental[i]);
            }

            str = null;
            str = buildTableRow(params);

            for (var i = 0; i < cRental.length; i++) {
               var graphic = cRental[i].features;
               graphic.setSymbol(rentSym);
               graphic.setInfoTemplate(rentTemp);
               if (cRental[i].serv.URL != undefined) {
                  var tempITemp = new esri.InfoTemplate(rentTemp.title, rentTemp.content + '<br/><a href="' + cRental[i].serv.URL + '">website</a>');
                  graphic.setInfoTemplate(tempITemp);
               }
               searchMapLayer.add(graphic);
            }
            dojo.attr(rentalRow, 'innerHTML', str);
         }
         else {
            wipeRentalRowOut.play();
            dojo.attr(rentalRow, 'class', 'notDisplayed');
         }
      }

      else {
         dojo.attr(rentalRow, 'innerHTML', '');
         wipeRentalRowOut.play();
         dojo.attr(rentalRow, 'class', 'notDisplayed');
      }
   }   
   
   var str = "";
   cTrash = closeByCenter(trashes);
   str = '<td><strong>Trash Pickup</strong></td><td>Day of Pickup:</td><td colspan="2">' + cTrash.serv["DAY"] + '</td>';
   dojo.attr(trashRow, 'innerHTML', str);
   dojo.attr(trashRow, 'class', 'isDisplayed');

   var url = "";

   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Historical District") {
         url = websiteList[i].attributes.URL;
      }
   }


   cHist = closeByCenter(hists);
   str = '<td><strong>Historical District</strong></td><td colspan="2">' + cHist.serv["DESCR"] + ' <a href="' + url + '" target="_blank">website</a></td>';
   dojo.attr(hDistRow, 'innerHTML', str);

   str = "";
   url = "";

   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Zoning") {
         url = websiteList[i].attributes.URL;
      }
   }

   cZone = closeByCenter(zones);
   str = '<td><strong>Zoning</strong></td><td colspan="2">' + cZone.serv["ZONE"] + ' <a href="' + url + '" target="_blank"> website</a></td>';
   dojo.attr(zoneRow, 'innerHTML', str);

   str = "";
   url = "";

   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Rental Inspection Districts") {
         url = websiteList[i].attributes.URL;
      }
   }
   cRental = closeByCenter(rentals);
   str = '<td><strong>Rental Inspection Districts</strong></td><td colspan="2">' + cRental.serv["NAME"] + ' <a href="' + url + '" target="_blank"> website</a></td>';
   dojo.attr(rentalRow, 'innerHTML', str);

   str = "";
   url = "";   
   
   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Hub Zone") {
         url = websiteList[i].attributes.URL;
      }
   }
   cHub = closeByCenter(hubs);
   if (inFeature(hubs)) {
      str = '<td><strong>Hub Zone</strong></td><td colspan="2">Center of Map Within SBA HUB Zone <a href="' + url + '" target="_blank">website</a></td>';
   }
   else {
      str = '<td><strong>Hub Zone</strong></td><td colspan="2">Center of Map Not Within SBA HUB Zone <a href="' + url + '" target="_blank">website</a></td>';

   }

   dojo.attr(hubRow, 'innerHTML', str);

   str = "";
   url = "";
   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Enterprise Zone") {
         url = websiteList[i].attributes.URL;
      }
   }
   cEZone = closeByCenter(eZones);
   if (inFeature(eZones)) {
      if (cEZone.serv["CLASS"] === 'ez') {
         str = '<td><strong>Enterprise Zone</strong></td><td colspan="2">Center of Map Within Enterprise Zone <a href="' + url + '" target="_blank">website</a></td>';
      }
      else {
         str = '<td><strong>Enterprise Zone</strong></td><td colspan="2">Center of Map Within Urban Enterprise Zone <a href="' + url + '" target="_blank">website</a></td>';
      }
   }
   else {
      str = '<td><strong>Enterprise Zone</strong></td><td colspan="2">Center of Map Not Within Enterprise Zone <a href="' + url + '" target="_blank">website</a></td>';
   }
   dojo.attr(eZoneRow, 'innerHTML', str);


   str = "";
   url = "";
   cCBlock = closeByCenter(cBlocks);
   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Census Block") {
         url = websiteList[i].attributes.URL;
      }
   }
   str = '<td><strong>Census Block</strong></td><td colspan="2">' + cCBlock.serv["NAME10"] + ' <a href="' + url + '" target="_blank">website</a></td>';
   dojo.attr(cBlockRow, 'innerHTML', str);

   polyParams.mapExtent = map.extent;
   polyParams.geometry = map.extent.getCenter();
   polyParams.layerIds = [43];
   polyId.execute(polyParams, getFloodInfo);

   if (wanted) {
      var graphic = new esri.Graphic(point, wantSym, {}, wantITemp);
      searchMapLayer.add(graphic);
      map.infoWindow.setContent(graphic.getContent());
      map.infoWindow.setTitle(graphic.getTitle());
      map.infoWindow.show(point);
   }
   setTableColoring();
}

/*function getTrashInfo(results) {
var str = '<td><strong>Trash Pickup</strong></td><td>Day of Pickup:</td><td>Unavailable</td>';
for (var i = 0; i < results.length; i++) {
str = '<td><strong>Trash Pickup</strong></td><td>Day of Pickup:</td><td colspan="2">' + results[i].feature.attributes["DAY"] + '</td>';
}
dojo.attr(trashRow, 'innerHTML', str);
}

function getHDistInfo(results) {
var url = "";
for (var i = 0; i < websiteList.length; i ++) {
if (websiteList[i].attributes.FEATURE === "Historic Districts") {
url = websiteList[i].attributes.URL;
}
}
var str = '<td><strong>Historical District</strong></td><td colspan="2">Not Within Historical District <a href="'+ url + '" target="_blank">website</a></td>';
for (var i = 0; i < results.length; i++) {
str = '<td><strong>Historical District</strong></td><td colspan="2">' + results[i].feature.attributes["DESCR"] + ' <a href="'+ url + '" target="_blank">website</a></td>';   
}
dojo.attr(hDistRow, 'innerHTML', str);
}*/

function getFloodInfo(results) {
   var url = "";
   for (var i = 0; i < websiteList.length; i++) {
      if (websiteList[i].attributes.FEATURE === "Flood Zone") {
         url = websiteList[i].attributes.URL;
      }
   }
   var str = '<td><strong>Flood Zone</strong></td><td colspan="2">Not In Flood Zone <a href="' + url + '" target="_blank">website</a></td>';
   for (var i = 0; i < results.length; i++) {
      str = '<td><strong>Flood Zone</strong></td><td colspan="2">' + results[i].feature.attributes["FLD_ZONE"] + ' <a href="' + url + '">website</a></td>';
   }
   dojo.attr(fZoneRow, 'innerHTML', str);
} /*

function getZoneInfo(results) {
   var url = "";
   for (var i = 0; i < websiteList.length; i ++) {
      if (websiteList[i].attributes.FEATURE === "Zoning") {
         url = websiteList[i].attributes.URL;
      }
   }
   var str = '<td><strong>Zoning</strong></td><td colspan="2">No Zoning Info Available <a href="'+ url + '" target="_blank">website</a></td>';
   for (var i = 0; i < results.length; i ++) {
      str = '<td><strong>Zoning</strong></td><td>'+ results[i].feature.attributes["ZONE"] + ' <a href="'+ url + '" target="_blank">website</a></td><td> ' + results[i].feature.attributes["ORDINANCE"] + '</td>';
   }
   dojo.attr(zoneRow, 'innerHTML', str);
}

function getHubInfo(results) {
   var url = "";
   for (var i = 0; i < websiteList.length; i ++) {
      if (websiteList[i].attributes.FEATURE === "SBA Hub Zones") {
         url = websiteList[i].attributes.URL;
      }
   }
   var str = '<td><strong>Hub Zone</strong></td><td colspan="2">Center of Map Not Within SBA Hub Zone <a href="'+ url + '" target="_blank">website</a></td>';
   if (results.length >0) {
      str = '<td><strong>Hub Zone</strong></td><td colspan="2">Center of Map Within SBA Hub Zone <a href="'+ url + '" target="_blank">website</a></td>';
   }
   dojo.attr(hubRow, 'innerHTML', str);
}

function getEZoneInfo(results) {
   var url = "";
   for (var i = 0; i < websiteList.length; i ++) {
      if (websiteList[i].attributes.FEATURE === "Enterprise Zones") {
         url = websiteList[i].attributes.URL;
      }
   }
   var str = '<td><strong>Enterprise Zone</strong></td><td colspan="2">Standard Enterprise Zone <a href="'+ url + '" target="_blank">website</a></td>';
   for (var i = 0; i < results.length; i ++) {
      str = '<td><strong>Enterprise Zone</strong></td><td colspan="2">' + results[i].feature.attributes["CLASS"] + ' <a href="'+ url + '" target="_blank">website</a></td>';
   }
   dojo.attr(eZoneRow, 'innerHTML', str);
}

function getCBlockInfo(results) {
   var url = "";
   for (var i = 0; i < websiteList.length; i ++) {
      if (websiteList[i].attributes.FEATURE === "2010 Census Blocks") {
         url = websiteList[i].attributes.URL;
      }
   }
   var str = '<td><strong>Census Block</strong></td><td colspan="2">No Census Block Information <a href="'+ url + '" target="_blank">website</a></td>';
   for (var i = 0; i < results.length; i ++) {
      str = '<td><strong>Census Block</strong></td><td colspan="2">' + results[i].feature.attributes["NAME10"] + ' <a href="'+ url + '" target="_blank">website</a></td>';
   }
   dojo.attr(cBlockRow, 'innerHTML', str);
}*/

function searchMap(evt) {
   if (searchTxt.textbox.value != null && searchTxt.textbox.value != "") {
      var text = searchTxt.textbox.value;
      params.layerIds = [];
      if (preSchool.checked)
         params.layerIds.push(16);
      if (eSchool.checked)
         params.layerIds.push(17);
      if (mSchool.checked)
         params.layerIds.push(18);
      if (hSchool.checked)
         params.layerIds.push(19);
      if (highEd.checked)
         params.layerIds.push(20);
      if (specSchool.checked)
         params.layerIds.push(21);
      if (privSchool.checked)
         params.layerIds.push(22);
      if (historic.checked)
         params.layerIds.push(1);
      if (recCent.checked)
         params.layerIds.push(2);
      if (muse.checked)
         params.layerIds.push(3);
      if (gov.checked)
         params.layerIds.push(4);
      if (nonProf.checked)
         params.layerIds.push(5);
      if (golf.checked)
         params.layerIds.push(6);
      if (lib.checked)
         params.layerIds.push(7);
      if (post.checked)
         params.layerIds.push(8);
      if (air.checked)
         params.layerIds.push(9);
      if (bus.checked)
         params.layerIds.push(10);
      if (train.checked)
         params.layerIds.push(11);
      if (grave.checked)
         params.layerIds.push(12);
      if (fStat.checked)
         params.layerIds.push(13);
      if (pStat.checked)
         params.layerIds.push(14);
      if (park.checked)
         params.layerIds.push(23);
      if (trail.checked)
         params.layerIds.push(24);
      if (beauty.checked)
         params.layerIds.push(27);
      if (repair.checked)
         params.layerIds.push(28);
      if (retail.checked)
         params.layerIds.push(29);
      if (shop.checked)
         params.layerIds.push(30);
      if (med.checked)
         params.layerIds.push(31);
      if (food.checked)
         params.layerIds.push(32);
      if (hotel.checked)
         params.layerIds.push(33);
      if (rec.checked)
         params.layerIds.push(34);
      if (eduServ.checked)
         params.layerIds.push(35);
      if (bank.checked)
         params.layerIds.push(36);
      if (finServ.checked)
         params.layerIds.push(37);
      if (manufact.checked)
         params.layerIds.push(38);
      if (contract.checked)
         params.layerIds.push(39);
      if (other.checked)
         params.layerIds.push(40);
      if (rental.checked)
         params.layerIds.push(40);

      var splitText = text.split("'");
      if (splitText.length > 1) {
         text = splitText[0];
      }

      params.searchText = text;
      searched = true;
      find.execute(params, function (results) { results.length > 0 ? findResults(results) : locateResults(text); });
   }
}

function findResults(results) {
   var autoFilledSearch = false;
   map.graphics.clear();
   var symbol = new esri.symbol.PictureMarkerSymbol('red-marker.png', 30, 30);
   var graphic;
   dojo.forEach(results, function (result) {
      if (result.geometryType == "esriGeometryPoint") {
         graphic = result.feature;
      }
      if (result.geometryType == "esriGeometryPolygon" || result.geometryType == "esriGeometryPolyline") {
         graphic = result.feature;
         graphic.setGeometry(result.feature.geometry.getExtent().getCenter());
      }
      graphic.setSymbol(symbol);
      switch (result.layerId) {
         case 1:
            graphic.setInfoTemplate(servITemp);
            break;
         case 2:
            graphic.setInfoTemplate(servITemp);
            break;
         case 3:
            graphic.setInfoTemplate(servITemp);
            break;
         case 4:
            graphic.setInfoTemplate(servITemp);
            break;
         case 5:
            graphic.setInfoTemplate(servITemp);
            break;
         case 6:
            graphic.setInfoTemplate(servITemp);
            break;
         case 7:
            graphic.setInfoTemplate(servITemp);
            break;
         case 8:
            graphic.setInfoTemplate(servITemp);
            break;
         case 9:
            graphic.setInfoTemplate(servITemp);
            break;
         case 10:
            graphic.setInfoTemplate(bSITemp);
            break;
         case 11:
            graphic.setInfoTemplate(servITemp);
            break;
         case 12:
            graphic.setInfoTemplate(servITemp);
            break;
         case 13:
            graphic.setInfoTemplate(servITemp);
            break;
         case 14:
            graphic.setInfoTemplate(servITemp);
            break;
         case 16:
            graphic.setInfoTemplate(schITemp);
            break;
         case 17:
            graphic.setInfoTemplate(schITemp);
            break;
         case 18:
            graphic.setInfoTemplate(schITemp);
            break;
         case 19:
            graphic.setInfoTemplate(schITemp);
            break;
         case 20:
            graphic.setInfoTemplate(schITemp);
            break;
         case 21:
            graphic.setInfoTemplate(schITemp);
            break;
         case 22:
            graphic.setInfoTemplate(schITemp);
            break;
         case 23:
            graphic.setInfoTemplate(pITemp);
            break;
         case 24:
            graphic.setInfoTemplate(trailITemp);
            break;
         case 27:
            graphic.setInfoTemplate(busITemp);
            break;
         case 28:
            graphic.setInfoTemplate(busITemp);
            break;
         case 29:
            graphic.setInfoTemplate(busITemp);
            break;
         case 30:
            graphic.setInfoTemplate(busITemp);
            break;
         case 31:
            graphic.setInfoTemplate(busITemp);
            break;
         case 32:
            graphic.setInfoTemplate(busITemp);
            break;
         case 33:
            graphic.setInfoTemplate(busITemp);
            break;
         case 34:
            graphic.setInfoTemplate(busITemp);
            break;
         case 35:
            graphic.setInfoTemplate(busITemp);
            break;
         case 36:
            graphic.setInfoTemplate(servITemp);
            break;
         case 37:
            graphic.setInfoTemplate(busITemp);
            break;
         case 38:
            graphic.setInfoTemplate(busITemp);
            break;
         case 39:
            graphic.setInfoTemplate(busITemp);
            break;
         case 40:
            graphic.setInfoTemplate(busITemp);
            break;
         default:
            graphic.setInfoTemplate();
            break;
      }
      map.graphics.add(graphic);
   });
   for (var i = 0; i < results.length; i++) {
      if (results[i].value === searchTxt.value) {
         map.infoWindow.setTitle(results[i].feature.getTitle());
         map.infoWindow.setContent(results[i].feature.getContent());
         map.infoWindow.show(results[i].feature.geometry);
         map.centerAt(results[i].feature.geometry);
         autoFilledSearch = true;
         break;
      }
   }
   if (!autoFilledSearch) {
      map.infoWindow.setTitle(results[0].feature.getTitle());
      map.infoWindow.setContent(results[0].feature.getContent());
      map.infoWindow.show(results[0].feature.geometry);
      map.centerAt(results[0].feature.geometry);
   }

}

function locateResults(searchTxt) {
   var address = {
      "Street": searchTxt,
      "City": "",
      "State": "",
      "ZIP": "",
      "f": "pjson"
   };
   lparams = { address: address, outFields: locOutFields };
   locate.addressToLocations(lparams, showLocate, errLocate);
}

function errLocate(result) {
   console.log("error accessing the geocoding service");
   clearSearch();
}

function showLocate(results) {
   map.graphics.clear();
   if (results.length > 0) {
      var symbol = new esri.symbol.PictureMarkerSymbol('red-marker.png', 30, 30);
      var graphic;
      for (var i = 0, l = results.length; i < l; i++) {
         locateITemp = new esri.InfoTemplate(results[i].address);
         graphic = new esri.Graphic();
         graphic.setGeometry(results[i].location);
         graphic.setInfoTemplate(locateITemp);
         graphic.setSymbol(symbol);
         map.graphics.add(graphic);
         map.infoWindow.setTitle("<br/>");
         map.infoWindow.setContent(results[0].address);
         map.infoWindow.show(results[0].location);
      }
      map.centerAt(results[0].location);
   }
   else {
      clearSearch();
   }
}

function clearSearch() {
   searched = false;
   wanted = false;
   searchTxt.set('value', "");
   map.infoWindow.hide();
   map.graphics.clear();
}

function clearInfo() {
   if (wanted) {
      wanted = false;
   }
   wantTo.set('item', null);
}

function calcDist(point1, point2) {
   var xdist = Math.abs(point1.x - point2.x);
   var ydist = Math.abs(point1.y - point2.y);
   var tdist = Math.sqrt((xdist * xdist) + (ydist * ydist));
   return tdist;
}

function buildTableRow(params) {
   var str = '<td align="left" valign="top"><strong>' + params.type + '</strong></td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0) {
         str = str + '<br/>';
      }
      if (params.features[i].serv[params.name] != null) {
         str = str + toTitleCase(params.features[i].serv[params.name].toLowerCase());
      }
      if (params.features[i].serv["URL"] != undefined) {
         str = str + ' <a href="' + params.features[i].serv["URL"] + '" target="_blank">website</a>';
      }
   }
   str = str + '</td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0) {
         str = str + '<br/>';
      }
      var test = params.features[i].serv[params.name].split("'");
      if (test.length > 1) {
         params.features[i].serv[params.name] = test[0] + test[1];
      }
      if (params.features[i].features.geometry.type != "polygon" && params.features[i].features.geometry.type != "polyline") {
         if (params.features[i].serv["URL"] != undefined) {
            str = str + '<a onClick=\'setMapCenter(' + params.features[i].features.geometry.x + ', ' + params.features[i].features.geometry.y + ', "'
               + params.features[i].serv[params.name] + '", "' + params.features[i].serv[params.address] + '", "' + params.features[i].serv["URL"] + '")\'>';
         }
         else {
            str = str + '<a onClick=\'setMapCenter(' + params.features[i].features.geometry.x + ', ' + params.features[i].features.geometry.y + ', "'
               + params.features[i].serv[params.name] + '", "' + params.features[i].serv[params.address] + '")\'>';
         }
      }
      else {
         if (params.features[i].serv["URL"] != undefined) {
            str = str + '<a onClick=\'setMapCenter(' + params.features[i].features.geometry.getExtent().getCenter().x + ', ' + params.features[i].features.geometry.getExtent().getCenter().y + ', \"'
               + params.features[i].serv[params.name] + '\", \"\", \"' + params.features[i].serv["URL"] + '\")\'>';
         }
         else {
            str = str + '<a onClick=\'setMapCenter(' + params.features[i].features.geometry.getExtent().getCenter().x + ', ' + params.features[i].features.geometry.getExtent().getCenter().y + ', \"'
               + params.features[i].serv[params.name] + '\")\'>';
         }
      }
      if (params.address != null) {
         str = str + toTitleCase(params.features[i].serv[params.address].toLowerCase()) + '</a>';
      }
      else {
         if (params.add1 === undefined) {
            if (params.type === "Park") {
               str = str + 'Go To Park</a>';
            }
            if (params.type === "Trail") {
               str = str + 'Go To Trail</a>';
            }
         }
         else {
            str = str + toTitleCase(params.features[i].serv[params.add1].toLowerCase()) + ', ' + toTitleCase(params.features[i].serv[params.add2].toLowerCase()) + '</a>';
         }
      }
   }
   str = str + '</td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0) {
         str = str + '<br/>';
      }
      str = str + '<a href="http://maps.google.com/maps?daddr=' + params.features[i].serv[params.googleAddress] + ', Danville, VA&hl=en" target="_blank">' + params.features[i].dist + ' Miles</a>';
   }
   str = str + '</td>';
   return str;
}

function setMapCenter(x, y, name, address, url) {
   contentString = '<strong>' + name + '</strong><br/><br/>';
   if (address != null && address != undefined) {
      contentString = contentString + address;
   }

   if (url != null && url != undefined) {
      contentString = contentString + '</br><a href="' + url + '">website</a>';
   }
   setTimeout(function () {
      map.infoWindow.hide();
      map.centerAt(new esri.geometry.Point(x, y, { wkid: 2284 }));
      map.infoWindow.setTitle('<br/>');
      map.infoWindow.setContent(contentString);
   }, 20);
   setTimeout(function () {
      map.infoWindow.show(new esri.geometry.Point(x, y, { wkid: 2288 }))
   }, 500);
}

function setTableColoring() {
   var counter = 1;
   for (var i = 0; i < rowList.length; i++) {
      if (dojo.attr(rowList[i], 'class') === "isDisplayed") {
         if (counter % 2 == 0) {
            dojo.attr(rowList[i], 'style', "background-color: white");
         }
         else {
            dojo.attr(rowList[i], 'style', "background-color: lightgrey");
         }
         counter = counter + 1;
      }
   }
}

dojo.ready(init);
dojo.ready(executeQueryTask);