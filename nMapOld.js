﻿dojo.require("esri.map");
dojo.require("dojo.fx");
dojo.require("esri.dijit.Popup");
dojo.require("esri.tasks.find");
dojo.require("esri.tasks.identify");
dojo.require("esri.tasks.locator");
dojo.require("esri.layers.FeatureLayer");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.AccordionContainer");
dojo.require("dijit.layout.AccordionPane");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.form.ToggleButton");
dojo.require("dijit.form.ComboBox");
dojo.require("dijit.form.FilteringSelect");
dojo.require("dijit.form.CheckBox");
dojo.require("dijit.form.RadioButton");
dojo.require("dojo.io.script");
dojo.require("dojo.data.ItemFileReadStore");

var map,
   popup,
   searchMapLayer,
   layerDefinition,
   leBody,
   spatRef,
   searchTxt,
   rowList,
   wantStore,
   searchList,
   searchStore,
   qTask,
   mapQuery,
   fparams,
   find,
   identify,
   locate,
   lparams,
   locOutFields,
   idparams,
   legend,

   wantITemp,
   locateITemp,

   servITemp,
   schITemp,
   bSITemp,
   trashITemp,
   pITemp,
   trailITemp,
   busITemp,
   cBTemp,
   eZTemp,
   fZTemp,
   histTemp,
   zTemp,
   hubTemp,

   wantSym,
   historSym,
   recCentSym,
   museSym,
   libSym,
   postSym,
   airSym,
   trainSym,
   graveSym,
   pSSym,
   golfSym,
   fSSym,
   preSSym,
   eSSym,
   mSSym,
   hSSym,
   highEdSym,
   specSSym,
   privSSym,
   bSSym,
   trashSym,
   beautySym,
   repairSym,
   retailSym,
   shopSym,
   foodSym,
   contractSym,
   medSym,
   hotelSym,
   manufactSym,
   eduSSym,
   finSym,
   recSym,
   otherSym,
   pSym,
   bTrailSym,
   hTrailSym,
   mTrailSym,
   cBGSym,
   cBSym,
   cTSym,
   eZSym,
   fSym,
   histSym,
   zSym,
   hubSym,
   parcSym,

   historics = [],
   recCents = [],
   muses = [],
   libs = [],
   posts = [],
   airs = [],
   trains = [],
   graves = [],
   pStats = [],
   golfs = [],
   shops = [],
   fStats = [],
   eSchools = [],
   mSchools = [],
   hSchools = [],
   specSchools = [],
   preSchools = [],
   highEds = [],
   privSchools = [],
   busResults = [],
   trashes = [],
   beauts = [],
   repairs = [],
   merches = [],
   foods = [],
   contracts = [],
   meds = [],
   hotels = [],
   manufacts = [],
   eduServs = [],
   finances = [],
   recs = [],
   others = [],
   parks = [],
   trails = [],
   cBlocks = [],
   tracts = [],
   eZones = [],
   floods = [],
   hists = [],
   zones = [],
   hubs = [],

   searched,
   wanted,
   zoomed,
   done,
   uniqueID,
   servChildren,
   schoolChildren,
   busChildren,
   commChildren;

function toTitleCase(str) {
	var A = str.split(' '), B = [];
	for (var i = 0; A[i] !== undefined; i++) {
		B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
	}
	return B.join(' ');
}

function init() {

   spatRef = new esri.SpatialReference(2284);

   //Create map and add the ArcGIS Online imagery layer
   esri.config.defaults.io.proxyUrl = "/arcgisserver/apis/javascript/proxy/proxy.ashx";
   var initialExtent = new esri.geometry.Extent({ "xmin": 11213809.978333335, "ymin": 3368595.863333334, "xmax": 11223212.756111111, "ymax": 3377581.974444445, "spatialReference": spatRef });
   popup = new esri.dijit.Popup({
      fillSymbol: new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 255, 0, 0.25]))
   }, dojo.create("div"));
   map = new esri.Map("mapDisplay", { infoWindow: popup, extent: initialExtent, logo: false }, { mapOptions: { slider: true} });
   dojo.connect(dijit.byId('mapDisplay'), 'resize', map, map.resize);

   var streetMapLayer = new esri.layers.ArcGISTiledMapServiceLayer("http://gis.danville-va.gov/ArcGIS/rest/services/Caches/Roads/MapServer");
   searchMapLayer = new esri.layers.GraphicsLayer();
   map.addLayers([streetMapLayer, searchMapLayer]);

   defineToc();
   defineWantList();
   defineFeatureLayers();
   connectEvents();
   uniqueID = 0;
   
   params = new esri.tasks.FindParameters();
   params.returnGeometry = true;
   idparams = new esri.tasks.IdentifyParameters();
   idparams.returnGeometry = true;
   idparams.layerIds = [2];
   idparams.tolerance = 3;
   idparams.spatialReference = spatRef;
   find = new esri.tasks.FindTask("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer");
   locate = new esri.tasks.Locator("http://geoapp/ArcGIS/rest/services/Locators/Address_Point_Locator/GeocodeServer");
   identify = new esri.tasks.IdentifyTask("http://gis.danville-va.gov/ArcGIS/rest/services/Main/MapServer");
   locOutFields = ["Shape", "Match_addr"];


   searched = false;
   wanted = false;
   zoomed = false;
   done = false;
   searchList = [];
   defineInfoTemplates();
   defineRows();
   //executeQueryTask();
}

function defineToc() {
   servChildren = [historic, recCent, muse, lib, post, air, train, grave, pStat, golf, fStat, edu, park, trail, bus, trash];
   schoolChildren = [eSchool, mSchool, hSchool, highEd, specSchool, preSchool, privSchool];
   busChildren = [beauty, repair, retail, shop, med, food, hotel, rec, eduServ, finServ, manufact, contract, other];
   commChildren = [cBlock, eZone, fZone, hDist, zone, hubZone];
}

function defineRows() {
   rowList = [
      historicRow, recCentRow, museRow,
      libRow, postRow, airRow, busRow,
      trainRow, graveRow, fStatRow, pStatRow,
      eSRow, mSRow, hSRow, highEdRow, specSRow,
      preSRow, privSRow, parkRow, golfRow,
      trailRow, trashRow, beautyRow, repairRow,
      retailRow, shopRow, medRow, foodRow, hotelRow,
      recRow, eduServRow, finServRow, manufactRow,
      contractRow, otherRow
      ];
}

function defineWantList() {
   wantStore = new dojo.data.ItemFileReadStore({
      url: "./wantList.json"
   });
   wantTo.set('store', wantStore);

   wantSym = new esri.symbol.PictureMarkerSymbol('blue-marker.png', 30, 30);
}

function defineFeatureLayers() {
   dojo.io.script.get({
      url: "http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/layers?f=json",
      callbackParamName: "callback",
      load: function (result) {
         layerDefinition = result;
         defineSymbology();
      },
      error: function () { alert("Unable to define the symbology for the symbols."); }
   });

   dojo.io.script.get({
      url: "http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/legend?f=json",
      callbackParamName: "callback",
      load: function (result) {
         legend = result;
         defineTOCImages();
      },
      error: function () { alert("Unable to load the Table of Contents symbology."); }
   });
}

function defineSymbology() {
   for (var i = 0; i < layerDefinition.layers.length; i ++) {
      if (layerDefinition.layers[i].name === 'Historic Spot') {
         historSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         historicImg.src = historSym.url;
      }
      if (layerDefinition.layers[i].name === 'Recreation Center') {
         recCentSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         recCentImg.src = recCentSym.url;
      }
      if (layerDefinition.layers[i].name === 'Museum') {
         museSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         museImg.src = museSym.url;
      }
      if (layerDefinition.layers[i].name === 'Library') {
         libSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         libImg.src = libSym.url;
      }
      if (layerDefinition.layers[i].name === 'Post Office') {
         postSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         postImg.src = postSym.url;
      }
      if (layerDefinition.layers[i].name === 'Airport') {
         airSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         airImg.src = airSym.url;
      }
      if (layerDefinition.layers[i].name === 'Bus Stop') {
         bSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         bSImg.src = bSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Train Station') {
         trainSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         trainImg.src = trainSym.url;
      }
      if (layerDefinition.layers[i].name === 'Cemetery') {
         graveSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         graveImg.src = graveSym.url;
      }
      if (layerDefinition.layers[i].name === 'Fire Station') {
         fSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         fStatImg.src = fSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Police Station') {
         pSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         pStatImg.src = pSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Preschool') {
         preSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         preSchoolImg.src = preSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Elementary') {
         eSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         eduImg.src = eSSym.url;
         eSchoolImg.src = eSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Middle') {
         mSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         mSchoolImg.src = mSSym.url;
      }
      if (layerDefinition.layers[i].name === 'High/Secondary') {
         hSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         hSchoolImg.src = hSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Higher Education') {
         highEdSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         highEdImg.src = highEdSym.url;
      }
      if (layerDefinition.layers[i].name === 'Specialty') {
         specSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         specSchoolImg.src = specSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Private') {
         privSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         privSchoolImg.src = privSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Park')
         pSym = new esri.symbol.SimpleFillSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
      if (layerDefinition.layers[i].name === 'Golf') {
         golfSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         golfImg.src = golfSym.url;
      }
      if (layerDefinition.layers[i].name === 'Trails') {
         bTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[0].symbol);
         hTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[1].symbol);
         mTrailSym = new esri.symbol.SimpleLineSymbol(layerDefinition.layers[i].drawingInfo.renderer.uniqueValueInfos[2].symbol);
      }
      if (layerDefinition.layers[i].name === 'Beauty/Barber') {
         beautySym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         beautyImg.src = beautySym.url;
      }
      if (layerDefinition.layers[i].name === 'Repair') {
         repairSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         repairImg.src = repairSym.url;
      }
      if (layerDefinition.layers[i].name === 'Retail') {
         retailSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         retailImg.src = retailSym.url;
      }
      if (layerDefinition.layers[i].name === 'Shopping Center') {
         shopSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         shopImg.src = shopSym.url;
      }
      if (layerDefinition.layers[i].name === 'Health Care') {
         medSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         medImg.src = medSym.url;
      }
      if (layerDefinition.layers[i].name === 'Restaurants') {
         foodSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         foodImg.src = foodSym.url;
      }
      if (layerDefinition.layers[i].name === 'Room/Board') {
         hotelSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         hotelImg.src = hotelSym.url;
      }
      if (layerDefinition.layers[i].name === 'Recreation/Leisure') {
         recSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         recImg.src = recSym.url;
      }
      if (layerDefinition.layers[i].name === 'Education Services') {
         eduSSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         eduServImg.src = eduSSym.url;
      }
      if (layerDefinition.layers[i].name === 'Financial Services') {
         finSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         finServImg.src = finSym.url;
      }
      if (layerDefinition.layers[i].name === 'Manufacturing') {
         manufactSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         manufactImg.src = manufactSym.url;
      }
      if (layerDefinition.layers[i].name === 'Contractors') {
         contractSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         contractImg.src = contractSym.url;
      }
      if (layerDefinition.layers[i].name === 'Other Services') {
         otherSym = new esri.symbol.PictureMarkerSymbol(layerDefinition.layers[i].drawingInfo.renderer.symbol);
         otherImg.src = otherSym.url;
      }
   }
}

function defineTOCImages() {
   for (var i = 0, l = legend.layers.length; i < l; i++) {
      if (legend.layers[i].layerName == 'Park')
         parkImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'Trails') {
         trailImg.src = "data:image/png;base64," + legend.layers[i].legend[2].imageData;
         bikeTrl.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
         hikeTrl.src = "data:image/png;base64," + legend.layers[i].legend[1].imageData;
         walkTrl.src = "data:image/png;base64," + legend.layers[i].legend[2].imageData;
      }
      if (legend.layers[i].layerName == 'Trash Pickup')
         trashImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == '2010 Census Blocks')
         cBImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'Enterprise Zones')
         eZImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'Flood Zones')
         fZImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'Historic Districts')
         hDImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'Zoning')
         zImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
      if (legend.layers[i].layerName == 'SBA Hub Zones')
         hZImg.src = "data:image/png;base64," + legend.layers[i].legend[0].imageData;
   }
}

function defineTOCSwatch() {
   parcSym = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID,
      new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
      new dojo.Color([255, 0, 0, 0.55]), 2), new dojo.Color([230, 152, 0, 255]));
}

function connectEvents() {
   dojo.connect(map, "onPanEnd", newCenterHandler);
   dojo.connect(map, "onZoomEnd", zoomChange);
   dojo.connect(dijit.byId('mapDisplay'), "resize", newCenterHandler);
   dojo.connect(searchBtn, "onClick", searchMap);
   dojo.connect(searchTxt, "onKeyDown", checkSearch);
   dojo.connect(clearBtn, "onClick", clearSearch);
   dojo.connect(wantTo, "onChange", getWant);
   dojo.connect(map.infoWindow, "onHide", clearInfo);
   dojo.connect(layers, "onClick", changeHeaders);
   dojo.connect(servBtn, "onChange", wipeServ);
   servBtn.attr('class', 'dijitTreeExpandoOpened');
   dojo.connect(businBtn, "onChange", wipeBus);
   businBtn.attr('class', 'dijitTreeExpandoOpened');
   dojo.connect(commBtn, "onChange", wipeComm);
   commBtn.attr('class', 'dijitTreeExpandoOpened');
   dojo.connect(schoolsBtn, "onChange", wipeSchools);
   dojo.connect(trlImgBtn, "onChange", wipeTrails);
   schoolsBtn.set('checked', false);
   trlImgBtn.set('checked', false);
   dojo.connect(serv, "onChange", changeServChildren);
   dojo.connect(edu, "onChange", changeEduChildren);
   dojo.connect(business, "onChange", changeBusChildren);
   dojo.connect(comm, "onChange", changeCommChildren);
   for (var i = 0, l = servChildren.length; i < l; i++)
      dojo.connect(servChildren[i], "onChange", newCenterHandler);
   for (var i = 0, l = schoolChildren.length; i < l; i++)
      dojo.connect(schoolChildren[i], "onChange", newCenterHandler);
   for (var i = 0, l = busChildren.length; i < l; i++)
      dojo.connect(busChildren[i], "onChange", newCenterHandler);
   for (var i = 0, l = commChildren.length; i < l; i++)
      dojo.connect(commChildren[i], "onChange", newCenterHandler);
}

function changeHeaders() {
   if (layers.label === 'Display None') {
      layers.set('label', 'Display All');
      serv.set('checked', false);
      business.set('checked', false);
      comm.set('checked', false);
      serv.set('disabled', true);
      business.set('disabled', true);
      comm.set('disabled', true);
   }
   else {
      layers.set('label', 'Display None');
      serv.set('disabled', false);
      business.set('disabled', false);
      comm.set('disabled', false);
      serv.set('checked', true);
      business.set('checked', true);
      comm.set('checked', true);
      for (var i = 0; i < servChildren.length; i++)
         if (servChildren[i].checked === false)
            servChildren[i].set('checked', true);

      for (var i = 0; i < schoolChildren.length; i++)
         if (schoolChildren[i].checked === false)
            schoolChildren[i].set('checked', true);

      for (var i = 0; i < busChildren.length; i++)
         if (busChildren[i].checked === false)
            busChildren[i].set('checked', true);

      for (var i = 0; i < commChildren.length; i++)
         if (commChildren[i].checked === false)
            commChildren[i].set('checked', true);
   }
}

function changeServChildren() {
   if (!serv.checked) {
      for (var i = 0, l = servChildren.length; i < l; i++) {
         servChildren[i].set('disabled', true);
      }
   }
   else {
      for (var i = 0, l = servChildren.length; i < l; i++) {
         servChildren[i].set('disabled', false);
      }
   }
   newCenterHandler();
}

function changeEduChildren() {
   if (!edu.checked) {
      schoolsBtn.set('checked', false);
      for (var i = 0, l = schoolChildren.length; i < l; i++) {
         schoolChildren[i].set('disabled', true);
      }
   }
   else {
      for (var i = 0, l = schoolChildren.length; i < l; i++) {
         schoolChildren[i].set('disabled', false);
      }
   }
   newCenterHandler();
}

function changeBusChildren() {
   if (!business.checked) {
      for (var i = 0, l = busChildren.length; i < l; i++) {
         busChildren[i].set('disabled', true);
      }
   }
   else {
      for (var i = 0, l = busChildren.length; i < l; i++) {
         busChildren[i].set('disabled', false);
      }
   }
   newCenterHandler();
}

function changeCommChildren() {
   if (!comm.checked) {
      for (var i = 0, l = commChildren.length; i < l; i++) {
         commChildren[i].set('disabled', true);
      }
   }
   else {
      for (var i = 0, l = commChildren.length; i < l; i++) {
         commChildren[i].set('disabled', false);
      }
   }
   newCenterHandler();
}

function wipeSchools() {
   if (schoolsBtn.checked) {
      schoolsBtn.attr('class', 'dijitTreeExpandoOpened');
      dojo.fx.wipeIn({
         node: schoolsNode,
         duration: 20
      }).play();
   }
   else {
      schoolsBtn.attr('class', 'dijitTreeExpandoClosed');
      dojo.fx.wipeOut({
         node: schoolsNode,
         duration: 20
      }).play();
   }
}

function wipeServ() {
   if (servBtn.checked) {
      servBtn.attr('class', 'dijitTreeExpandoOpened');
      dojo.fx.wipeIn({
         node: servNode,
         duration: 20
      }).play();
   }
   else {
      servBtn.attr('class', 'dijitTreeExpandoClosed');
      dojo.fx.wipeOut({
         node: servNode,
         duration: 20
      }).play();
   }
}

function wipeBus() {
   if (businBtn.checked) {
      businBtn.attr('class', 'dijitTreeExpandoOpened');
      dojo.fx.wipeIn({
         node: businessNode,
         duration: 20
      }).play();
   }
   else {
      businBtn.attr('class', 'dijitTreeExpandoClosed');
      dojo.fx.wipeOut({
         node: businessNode,
         duration: 20
      }).play();
   }
}

function wipeComm() {
   if (commBtn.checked) {
      commBtn.attr('class', 'dijitTreeExpandoOpened');
      dojo.fx.wipeIn({
         node: commNode,
         duration: 20
      }).play();
   }
   else {
      commBtn.attr('class', 'dijitTreeExpandoClosed');
      dojo.fx.wipeOut({
         node: commNode,
         duration: 20
      }).play();
   }
}

function wipeTrails() {
   if (trlImgBtn.checked) {
      trlImgBtn.attr('class', 'dijitTreeExpandoOpened');
      dojo.fx.wipeIn({
         node: trailList,
         duration: 20
      }).play();
   }
   else {
      trlImgBtn.attr('class', 'dijitTreeExpandoClosed');
      dojo.fx.wipeOut({
         node: trailList,
         duration: 20
      }).play();
   }
}

function checkSearch(evt) {
   if (evt.keyCode === dojo.keys.ENTER)
      searchMap();
   else
      if(evt.keyCode == dojo.keys.DELETE || evt.keyCode == dojo.keys.BACKSPACE)
         searchTxt.closeDropDown();
}

function getWant() {
   if (wantTo.item != null) {
      wanted = true;
      searchMapLayer.clear();
      var point = new esri.geometry.Point(parseFloat(wantTo.item.x), parseFloat(wantTo.item.y), spatRef);
      if (wantTo.item.photoUrl != "") {
         wantITemp = new esri.InfoTemplate("<strong>" + wantTo.item.name + "</strong>", '<table><tr><td><a href="' + wantTo.item.photo_url + '" target="_blank"><img src="' + wantTo.item.photo_url + '" width="50" height="50"/></a></td><td>' + wantTo.item.address + '<br/><a href="'  + wantTo.item.webpage_url + '" target="_blank">Website</td></tr><tr><td></td><td>' + wantTo.item.desc + '</td></tr></table>');
      }
      else {
         wantITemp = new esri.InfoTemplate("<strong>" + wantTo.item.name + "</strong>", '<table><tr><td>' + wantTo.item.address + '<br/><a href="' + wantTo.item.webpage_url + '" target="_blank">Website</td></tr><tr><td>' + wantTo.item.desc + '</td></tr></table>');
      }
      var graphic = new esri.Graphic(point, wantSym, {}, wantITemp);
      map.graphics.add(graphic);
      map.infoWindow.setContent(graphic.getContent());
      map.infoWindow.setTitle(graphic.getTitle());
      map.infoWindow.show(point);
      map.centerAt(point);
   }
}

function getOldQuery(url, outputFields, qParam, callBackFunction) {
   qTask = new esri.tasks.QueryTask(url);

   mapQuery = new esri.tasks.Query();
   mapQuery.returnGeometry = true;
   mapQuery.outFields = outputFields;
   mapQuery.where = qParam;
   qTask.execute(mapQuery, callBackFunction).then(newCenterHandler);
}

function getQuery(url, outputFields, qParam, resultSet, searchFields) {
   qTask = new esri.tasks.QueryTask(url);

   mapQuery = new esri.tasks.Query();
   mapQuery.returnGeometry = true;
   mapQuery.outFields = outputFields;
   mapQuery.where = qParam;
   qTask.execute(mapQuery, function (fSet) {
      for (var i = 0; i < fSet.features.length; i++) {
         resultSet.push(fSet.features[i]);
      }
      for (var i = 0; i < resultSet.length; i++) {
         if (searchFields != null && searchFields != undefined) {
            for (var j = 0; j < searchFields.length; j++) {
               searchList.push({ val: resultSet[i].attributes[searchFields[j]] });
            }
         }
      }
   }).then(newCenterHandler);
}

function defineInfoTemplates() {
   servITemp = new esri.InfoTemplate("<br/>", "<strong>${NAME}</strong><br/> ${DESCRIPTIO}<br/><br/><strong>Address: </strong>${ADDRESS}");
   schITemp = new esri.InfoTemplate("<br/>", "<strong>${SCHOOL}</strong><br/>${TYPE}<br/><br/><strong>Address: </strong>${ADDRESS}");
   bSITemp = new esri.InfoTemplate("<br/>", "<strong>${ADDRESS}</strong><br/>${DESCRIPTION}");
   pITemp = new esri.InfoTemplate("<br/>", "<strong>${Name}</strong><br/>");
   trailITemp = new esri.InfoTemplate("<br/>", "<strong>${TRAIL_NAME}</strong><br/>Type: ${TRAIL_TYPE}<br/> Difficulty: ${DIFFICULTY}<br/> Paved: ${PAVED}<br/>");
   busITemp = new esri.InfoTemplate("<br/>", "<strong>${AccountNam}</strong><br/>${SimpDesc}<br/><br/> <strong>Address: </strong>${Location}");
}

function executeQueryTask() {
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/1", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", historics, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/2", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", recCents, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/3", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", muses, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/4", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", libs, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/5", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", posts, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/6", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", airs, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/7", ["OBJECTID", "DESCRIPTION", "ADDRESS"], "1=1", busResults, ["ADDRESS"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/8", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", trains, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/9", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", graves, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/10", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", fStats, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/11", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", pStats, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/14", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", eSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/15", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", mSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/16", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", hSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/17", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", highEds, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/18", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", specSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/13", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", preSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/19", ["OBJECTID", "SCHOOL", "ADDRESS", "TYPE"], "1=1", privSchools, ["ADDRESS", "SCHOOL"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/20", ["OBJECTID", "Name"], "1=1", parks, ["NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/21", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", golfs, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/22", ["OBJECTID", "TRAIL_NAME", "TRAIL_TYPE", "DIFFICULTY", "PAVED"], "1=1", trails, ["TRAIL_NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/23", ["OBJECTID", "ZONE", "DAY"], "1=1", trashes);

   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/44", ["OBJECTID", "NAME10"], "BLOCKCE10 LIKE '10%'", cBlocks);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/44", ["OBJECTID", "NAME10"], "BLOCKCE10 LIKE '20%'", cBlocks);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/44", ["OBJECTID", "NAME10"], "BLOCKCE10 LIKE '30%'", cBlocks);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/44", ["OBJECTID", "NAME10"], "BLOCKCE10 LIKE '40%'", cBlocks);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/43", ["OBJECTID", "CLASS"], "1=1", eZones);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/40", ["OBJECTID", "FLD_ZONE"], "1=1", floods);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/39", ["OBJECTID", "DESCR"], "1=1", hists);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/41", ["OBJECTID", "ZONE", "ORDINANCE"], "1=1", zones);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/42", ["OBJECTID", "TRACT"], "1=1", hubs);

   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/25", ["SimpDesc", "AccountNam", "Location"], "1=1", beauts, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/26", ["SimpDesc", "AccountNam", "Location"], "1=1", repairs, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/27", ["SimpDesc", "AccountNam", "Location"], "1=1", merches, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/28", ["OBJECTID", "NAME", "ADDRESS", "DESCRIPTIO"], "1=1", shops, ["ADDRESS", "NAME"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/29", ["SimpDesc", "AccountNam", "Location"], "1=1", meds, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/30", ["SimpDesc", "AccountNam", "Location"], "1=1", foods, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/31", ["SimpDesc", "AccountNam", "Location"], "1=1", hotels, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/35", ["SimpDesc", "AccountNam", "Location"], "1=1", manufacts, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/36", ["SimpDesc", "AccountNam", "Location"], "1=1", contracts, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/33", ["SimpDesc", "AccountNam", "Location"], "1=1", eduServs, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/34", ["SimpDesc", "AccountNam", "Location"], "1=1", finances, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/32", ["SimpDesc", "AccountNam", "Location"], "1=1", recs, ["AccountNam", "Location"]);
   getQuery("http://gis.danville-va.gov/ArcGIS/rest/services/myNeighborhood/MapServer/37", ["SimpDesc", "AccountNam", "Location"], "1=1", others, ["AccountNam", "Location"]);

}

/*function setHistorResults(fSet) {
   historics = fSet.features;
   for (var i = 0, l = historics.length; i < l; i++) {
      searchList.push({ val: historics[i].attributes.ADDRESS });
      searchList.push({ val: historics[i].attributes.NAME });
   }
}

function setRecCentResults(fSet) {
   recCents = fSet.features;
   for (var i = 0, l = recCents.length; i < l; i++) {
      searchList.push({ val: recCents[i].attributes.ADDRESS });
      searchList.push({ val: recCents[i].attributes.NAME });
   }
}

function setMuseResults(fSet) {
   muses = fSet.features;
   for (var i = 0, l = muses.length; i < l; i++) {
      searchList.push({ val: muses[i].attributes.ADDRESS });
      searchList.push({ val: muses[i].attributes.NAME });
   }
}

function setLibResults(fSet) {
   libs = fSet.features;
   for (var i = 0, l = libs.length; i < l; i++) {
      searchList.push({ val: libs[i].attributes.ADDRESS });
      searchList.push({ val: libs[i].attributes.NAME });
   }
}

function setPostResults(fSet) {
   posts = fSet.features;
   for (var i = 0, l = posts.length; i < l; i++) {
      searchList.push({ val: posts[i].attributes.ADDRESS });
      searchList.push({ val: posts[i].attributes.NAME });
   }
}

function setAirResults(fSet) {
   airs = fSet.features;
   for (var i = 0, l = airs.length; i < l; i++) {
      searchList.push({ val: airs[i].attributes.ADDRESS });
      searchList.push({ val: airs[i].attributes.NAME });
   }
}

function setTrainResults(fSet) {
   trains = fSet.features;
   for (var i = 0, l = trains.length; i < l; i++) {
      searchList.push({ val: trains[i].attributes.ADDRESS });
      searchList.push({ val: trains[i].attributes.NAME });
   }
}

function setGraveResults(fSet) {
   graves = fSet.features;
   for (var i = 0, l = graves.length; i < l; i++) {
      searchList.push({ val: graves[i].attributes.ADDRESS });
      searchList.push({ val: graves[i].attributes.NAME });
   }
}

function setFStatResults(fSet) {
   fStats = fSet.features;
   for (var i = 0, l = fStats.length; i < l; i++) {
      searchList.push({ val: fStats[i].attributes.ADDRESS });
      searchList.push({ val: fStats[i].attributes.NAME });
   }
}

function setPStatResults(fSet) {
   pStats = fSet.features;
   for (var i = 0, l = pStats.length; i < l; i++) {
      searchList.push({ val: pStats[i].attributes.ADDRESS });
      searchList.push({ val: pStats[i].attributes.NAME });
   }
}

function setESchoolResults(fSet) {
   eSchools = fSet.features;
   for (var i = 0, l = eSchools.length; i < l; i++) {
      searchList.push({ val: eSchools[i].attributes.ADDRESS });
      searchList.push({ val: eSchools[i].attributes.SCHOOL });
   }
}

function setMSchoolResults(fSet) {
   mSchools = fSet.features;
   for (var i = 0, l = mSchools.length; i < l; i++) {
      searchList.push({ val: mSchools[i].attributes.ADDRESS });
      searchList.push({ val: mSchools[i].attributes.SCHOOL });
   }
}

function setSecSchoolResults(fSet) {
   hSchools = fSet.features;
   for (var i = 0, l = hSchools.length; i < l; i++) {
      searchList.push({ val: hSchools[i].attributes.ADDRESS });
      searchList.push({ val: hSchools[i].attributes.SCHOOL });
   }
}

function setSpecSchoolResults(fSet) {
   specSchools = fSet.features;
   for (var i = 0, l = specSchools.length; i < l; i++) {
      searchList.push({ val: specSchools[i].attributes.ADDRESS });
      searchList.push({ val: specSchools[i].attributes.SCHOOL });
   }
}

function setHighEdResults(fSet) {
   highEds = fSet.features;
   for (var i = 0, l = highEds.length; i < l; i++) {
      searchList.push({ val: highEds[i].attributes.ADDRESS });
      searchList.push({ val: highEds[i].attributes.SCHOOL });
   }
}

function setPreSchoolResults(fSet) {
   preSchools = fSet.features;
   for (var i = 0, l = preSchools.length; i < l; i++) {
      searchList.push({ val: preSchools[i].attributes.ADDRESS });
      searchList.push({ val: preSchools[i].attributes.SCHOOL });
   }
}

function setPrivSchoolResults(fSet) {
   privSchools = fSet.features;
   for (var i = 0, l = privSchools.length; i < l; i++) {
      searchList.push({ val: privSchools[i].attributes.ADDRESS });
      searchList.push({ val: privSchools[i].attributes.SCHOOL });
   }
   
}

function setBusResults(fSet) {
   busResults = fSet.features;
   for (var i = 0, l = busResults.length; i < l; i++)
      searchList.push({ val: busResults[i].attributes.ADDRESS });
}

function setTrashResults(fSet) {
   trashes = fSet.features;
}

function setBeautyResults(fSet) {
   beauts = fSet.features;
   for (var i = 0, l = beauts.length; i < l; i++) {
      searchList.push({ val: beauts[i].attributes.AccountNam });
      searchList.push({ val: beauts[i].attributes.Location });
   }
}

function setRepairResults(fSet) {
   repairs = fSet.features;
   for (var i = 0, l = repairs.length; i < l; i++) {
      searchList.push({ val: repairs[i].attributes.AccountNam });
      searchList.push({ val: repairs[i].attributes.Location });
   }
}

function setMerchResults(fSet) {
   merches = fSet.features;
   for (var i = 0, l = merches.length; i < l; i++) {
      searchList.push({ val: merches[i].attributes.AccountNam });
      searchList.push({ val: merches[i].attributes.Location });
   }
}

function setShopResults(fSet) {
   shops = fSet.features;
   for (var i = 0, l = shops.length; i < l; i++) {
      searchList.push({ val: shops[i].attributes.ADDRESS });
      searchList.push({ val: shops[i].attributes.NAME });
   }
}

function setHealthResults(fSet) {
   meds = fSet.features;
   for (var i = 0, l = meds.length; i < l; i++) {
      searchList.push({ val: meds[i].attributes.AccountNam });
      searchList.push({ val: meds[i].attributes.Location });
   }
}

function setFoodResults(fSet) {
   foods = fSet.features;
   for (var i = 0, l = foods.length; i < l; i++) {
      searchList.push({ val: foods[i].attributes.AccountNam });
      searchList.push({ val: foods[i].attributes.Location });
   }
}

function setHotelResults(fSet) {
   hotels = fSet.features;
   for (var i = 0, l = hotels.length; i < l; i++) {
      searchList.push({ val: hotels[i].attributes.AccountNam });
      searchList.push({ val: hotels[i].attributes.Location });
   }
}

function setManufactResults(fSet) {
   manufacts = fSet.features;
   for (var i = 0, l = manufacts.length; i < l; i++) {
      searchList.push({ val: manufacts[i].attributes.AccountNam });
      searchList.push({ val: manufacts[i].attributes.Location });
   }
}

function setContractResults(fSet) {
   contracts = fSet.features;
   for (var i = 0, l = contracts.length; i < l; i++) {
      searchList.push({ val: contracts[i].attributes.AccountNam });
      searchList.push({ val: contracts[i].attributes.Location });
   }
}

function setEduResults(fSet) {
   eduServs = fSet.features;
   for (var i = 0, l = eduServs.length; i < l; i++) {
      searchList.push({ val: eduServs[i].attributes.AccountNam });
      searchList.push({ val: eduServs[i].attributes.Location });
   }
}

function setFinanceResults(fSet) {
   finances = fSet.features;
   for (var i = 0, l = finances.length; i < l; i++) {
      searchList.push({ val: finances[i].attributes.AccountNam });
      searchList.push({ val: finances[i].attributes.Location });
   }
}

function setOtherResults(fSet) {
   others = fSet.features;
   for (var i = 0, l = others.length; i < l; i++) {
      searchList.push({ val: others[i].attributes.AccountNam });
      searchList.push({ val: others[i].attributes.Location });
   }
}

function setRecLeisureResults(fSet) {
   recs = fSet.features;
   for (var i = 0, l = recs.length; i < l; i++) {
      searchList.push({ val: recs[i].attributes.AccountNam });
      searchList.push({ val: recs[i].attributes.Location });
   }
}

function setParkResults(fSet) {
   parks = fSet.features;
   for (var i = 0, l = parks.length; i < l; i++)
      searchList.push({ val: parks[i].attributes.Name});
}

function setGolfResults(fSet) {
   golfs = fSet.features;
   for (var i = 0, l = golfs.length; i < l; i++) {
      searchList.push({ val: golfs[i].attributes.ADDRESS });
      searchList.push({ val: golfs[i].attributes.NAME });
   }
}

function setTrailResults(fSet) {
   trails = fSet.features;
   for (var i = 0, l = trails.length; i < l; i++)
      searchList.push({ val: trails[i].attributes.TRAIL_NAME });
   
   searchStore = new dojo.store.Memory({
      data: searchList
   });
   searchTxt.set('store', searchStore);
}

function setCensusBlockResults(fSet) {
   if (cBlocks == null)
      cBlocks = fSet.features;
   else {
      for (var i = 0, l = fSet.features.length; i < l; i++)
         cBlocks.push(fSet.features[i]);
   }
}

function setEntZoneResults(fSet) {
   eZones = fSet.features;
}

function setFloodResults(fSet) {
   floods = fSet.features;
}

function setHistResults(fSet) {
   hists = fSet.features;
}

function setZoneResults(fSet) {
   zones = fSet.features;
}

function setHubResults(fSet) {
   hubs = fSet.features;
}*/


function closeByPoint(fSetResults) {
   var dists = [];

   //Calculate the straight line distance between centerpoint and feature.
   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   //Convert distance to miles
   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   var returnObj = dists[0],
             smallest = dists[0].dist;
   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }

   uniqueID++;
   return returnObj;
}

function closeSetByPoint(fSetResults, num) {
   if (fSetResults != null && fSetResults != undefined) {
	var dists = [];
	var closest = [];

	//Calculate the straight line distance between centerpoint and feature.
	for (var i = 0, l = fSetResults.length; i < l; i++) {
		uniqueID++;
		dists.push({
			dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
			serv: fSetResults[i].attributes,
			features: fSetResults[i],
			uID: uniqueID
		});
	}

	//Convert distance to miles
	for (var i = 0, l = dists.length; i < l; i++) {
		dists[i].dist = (dists[i].dist / 5280).toFixed(2);
	}

	dists.sort(function (a, b) { return a.dist - b.dist });

	if (num == null || num == "")
		return [dists[0]];
	else {
		if (num > dists.length)
			num = dists.length;
		if (num < 0)
			num = 1;
		for (var i = 0; i < num; i++)
			closest.push(dists[i]);
	}

	uniqueID++;
	return closest;
   }
}

function closeSetByDist(fSetResults, rad) {
   var dists = [];
   var closest = [];

   //Calculate the straight line distance between centerpoint and feature.
   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   //Convert distance to miles
   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   dists.sort(function (a, b) { return a.dist - b.dist });

   if (rad == null || rad == "")
      return [dists[0]];
   else {
      for (var i = 0; i < dists.length; i++) {
         if ((+dists[i].dist) < (+rad))
            closest.push(dists[i]);
      }
   }

   uniqueID++;
   return closest;
}

function closeByCenter(fSetResults) {
   var dists = [],
             smallest,
             returnObj = null;

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
         uniqueID++;
         return {
            dist: 0,
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         };
      }
   }

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }


   returnObj = dists[0],
         smallest = dists[0].dist;
   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }
   return returnObj;
}

function closeCenterByDist(fSetResults, rad) {
   var dists = [],
       returnObj = [];

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   dists.sort(function (a, b) { return a.dist - b.dist });

   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(rad)) {
         returnObj.push(dists[i]);
      }
   }
   return returnObj;
}

function closeCenterByNum(fSetResults, num) {
   if (fSetResults != null && fSetResults != undefined) {
   var dists = [],
       returnObj = [];

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      uniqueID++;
      dists.push({
         dist: calcDist(map.extent.getCenter(), fSetResults[i].geometry.getExtent().getCenter()).toFixed(2),
         serv: fSetResults[i].attributes,
         features: fSetResults[i],
         uID: uniqueID
      });
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   dists.sort(function (a, b) { return a.dist - b.dist });

   for (i = 0, l = dists.length; i < l; i++) {
      if (i < num)
         returnObj.push(dists[i]);
   }
   return returnObj;
   }
}

function closeByOutline(fSetResults) {
   if (fSetResults != null && fSetResults != undefined) {
      var dists = [],
             smallest,
             returnObj = null;

      for (var i = 0, l = fSetResults.length; i < l; i++) {
         if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
            uniqueID++;
            return {
               dist: 0,
               serv: fSetResults[i].attributes,
               features: fSetResults[i],
               uID: uniqueID
            };
         }
      }

      for (var i = 0, l = fSetResults.length; i < l; i++) {
         for (var k = 0, j = fSetResults[i].geometry.rings.length; k < j; k++) {
            for (var m = 0, n = fSetResults[i].geometry.rings[k].length; m < n; m++) {
               uniqueID++;
               dists.push({
                  dist: calcDist(new esri.geometry.Point(fSetResults[i].geometry.rings[k][m][0], fSetResults[i].geometry.rings[k][m][1], spatRef), map.extent.getCenter()),
                  serv: fSetResults[i].attributes,
                  features: fSetResults[i],
                  uID: uniqueID
               });
            }
         }
      }

      for (var i = 0, l = dists.length; i < l; i++) {
         dists[i].dist = (dists[i].dist / 5280).toFixed(2);
      }


      returnObj = dists[0],
      smallest = dists[0].dist;
      for (i = 0, l = dists.length; i < l; i++) {
         if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
            smallest = dists[i].dist;
            returnObj = dists[i];
         }
      }

      return returnObj;
   }
}

function closeByPolyline(fSetResults) {
   var dists = [],
             smallest,
             returnObj = null;

   for (var i = 0, l = fSetResults.length; i < l; i++) {
      for (var j = 0, k = fSetResults[i].geometry.paths.length; j < k; j++) {
         uniqueID++;
         dists.push({ dist: calcDist(new esri.geometry.Point(fSetResults[i].geometry.paths[j][0], fSetResults[i].geometry.paths[j][1], spatRef), map.extent.getCenter()),
            serv: fSetResults[i].attributes,
            features: fSetResults[i],
            uID: uniqueID
         });
      }
   }

   for (var i = 0, l = dists.length; i < l; i++) {
      dists[i].dist = (dists[i].dist / 5280).toFixed(2);
   }

   returnObj = dists[0],
         smallest = dists[0].dist;
   for (i = 0, l = dists.length; i < l; i++) {
      if (parseFloat(dists[i].dist) < parseFloat(smallest)) {
         smallest = dists[i].dist;
         returnObj = dists[i];
      }
   }

   return returnObj;
}

function inFeature(fSetResults) {
   if (fSetResults != null && fSetResults != undefined) {
      for (var i = 0, l = fSetResults.length; i < l; i++) {
         if (fSetResults[i].geometry.contains(map.extent.getCenter())) {
            uniqueID++;
            return {
               dist: 0,
               serv: fSetResults[i].attributes,
               features: fSetResults[i],
               uID: uniqueID
            };
         }
      }
      uniqueID++;
      return { dist: 0, serv: { FLD_ZONE: "Not in Flood Zone", TRACT: "Center point not in Hub Zone" }, features: null, uID: uniqueID };
   }
}

function zoomChange(ext, zFactor, anch, level) {
   if (level > 7) {
      zoomed = true;
      searchMapLayer.clear();
      idparams.mapExtent = map.extent;
      idparams.geometry = map.extent.getCenter();
      identify.execute(idparams, pushIdResults);
   }
   else {
      zoomed = false;
      newCenterHandler();
   }
}

function pushIdResults(results) {
   for (var i = 0, l = results.length; i < l; i++) {
      var graphic = results[i].feature;
      var blank = "";
      var temp = new esri.InfoTemplate("<br/>", "<strong>Owner: </strong>" + results[i].feature.attributes["Owner 1"] + "<br/>"
            + "<br/><strong>Address: </strong>" + results[i].feature.attributes["Mailing Address 2"] + "<br/>"
            + "<br/><strong>Use Code: </strong>" + results[i].feature.attributes["Use Code"] + "<br/>"
            + '<br/><a href="http://gis.danville-va.gov/TestParcelViewer/ParcelViewer/?Key=' + results[i].feature.attributes["Parcel Account ID"]
            + '" target="_blank">View in Assessor Map</a>');
      graphic.setInfoTemplate(temp);
      graphic.setSymbol(parcSym);
      searchMapLayer.add(graphic);
   }
}


function newCenterHandler(evt) {
   if (!done)
      defineTOCSwatch();
   if (!zoomed) {
      if (!wanted && !searched) {
         searchMapLayer.clear();
      }
      else {
         wanted = false;
         searched = false;
      }
      var graphic,
         cHistorics,
         cRecCents,
         cMuses,
         cLibs,
         cPosts,
         cAirs,
         cTrains,
         cGraves,
         cPStats,
         cGolfs,
         cFStats,
         cESchools,
         cMSchools,
         cHSchools,
         cHighEds,
         cSpecSchools,
         cPreSchools,
         cPrivSchools,
         cBus,
         cTrash,
         cParks,
         cTrails,
         cCBlock,
         cEZone,
         cFlood,
         cHist,
         cZone,
         cHub,
         cBeauts,
         cRepairs,
         cMerches,
         cShops,
         cMeds,
         cFoods,
         cHotels,
         cManufacts,
         cContracts,
         cEdus,
         cFins,
         cRecs,
         cOthers;

      if (checkByDist.checked) {
         cHistorics = closeSetByDist(historics, radius.value);
         cRecCents = closeSetByDist(recCents, radius.value);
         cMuses = closeSetByDist(muses, radius.value);
         cLibs = closeSetByDist(libs, radius.value);
         cPosts = closeSetByDist(posts, radius.value);
         cAirs = closeSetByDist(airs, radius.value);
         cTrains = closeSetByDist(trains, radius.value);
         cGraves = closeSetByDist(graves, radius.value);
         cFStats = closeSetByDist(fStats, radius.value);
         cPStats = closeSetByDist(pStats, radius.value);
         cESchools = closeSetByDist(eSchools, radius.value);
         cMSchools = closeSetByDist(mSchools, radius.value);
         cHSchools = closeSetByDist(hSchools, radius.value);
         cHighEds = closeSetByDist(highEds, radius.value);
         cSpecSchools = closeSetByDist(specSchools, radius.value);
         cPreSchools = closeSetByDist(preSchools, radius.value);
         cPrivSchools = closeSetByDist(privSchools, radius.value);
         cParks = closeCenterByDist(parks, radius.value);
         cGolfs = closeSetByDist(golfs, radius.value);
         cTrails = closeCenterByDist(trails, radius.value);
         cBus = closeSetByDist(busResults, radius.value);
         cBeauts = closeSetByDist(beauts, radius.value);
         cRepairs = closeSetByDist(repairs, radius.value);
         cMerches = closeSetByDist(merches, radius.value);
         cShops = closeSetByDist(shops, radius.value);
         cMeds = closeSetByDist(meds, radius.value);
         cFoods = closeSetByDist(foods, radius.value);
         cHotels = closeSetByDist(hotels, radius.value);
         cRecs = closeSetByDist(recs, radius.value);
         cEdus = closeSetByDist(eduServs, radius.value);
         cFins = closeSetByDist(finances, radius.value);
         cManufacts = closeSetByDist(manufacts, radius.value);
         cContracts = closeSetByDist(contracts, radius.value);
         cOthers = closeSetByDist(others, radius.value);
      }
      else {
         if (maxFeatures.checked) {
            cHistorics = closeSetByPoint(historics, totalFeatures.value);
            cRecCents = closeSetByPoint(recCents, totalFeatures.value);
            cMuses = closeSetByPoint(muses, totalFeatures.value);
            cLibs = closeSetByPoint(libs, totalFeatures.value);
            cPosts = closeSetByPoint(posts, totalFeatures.value);
            cAirs = closeSetByPoint(airs, totalFeatures.value);
            cTrains = closeSetByPoint(trains, totalFeatures.value);
            cGraves = closeSetByPoint(graves, totalFeatures.value);
            cFStats = closeSetByPoint(fStats, totalFeatures.value);
            cPStats = closeSetByPoint(pStats, totalFeatures.value);
            cESchools = closeSetByPoint(eSchools, totalFeatures.value);
            cMSchools = closeSetByPoint(mSchools, totalFeatures.value);
            cHSchools = closeSetByPoint(hSchools, totalFeatures.value);
            cHighEds = closeSetByPoint(highEds, totalFeatures.value);
            cSpecSchools = closeSetByPoint(specSchools, totalFeatures.value);
            cPreSchools = closeSetByPoint(preSchools, totalFeatures.value);
            cPrivSchools = closeSetByPoint(privSchools, totalFeatures.value);
            cParks = closeCenterByNum(parks, totalFeatures.value);
            cGolfs = closeSetByPoint(golfs, totalFeatures.value);
            cTrails = closeCenterByNum(trails, totalFeatures.value);
            cBus = closeSetByPoint(busResults, totalFeatures.value);
            cBeauts = closeSetByPoint(beauts, totalFeatures.value);
            cRepairs = closeSetByPoint(repairs, totalFeatures.value);
            cMerches = closeSetByPoint(merches, totalFeatures.value);
            cShops = closeSetByPoint(shops, totalFeatures.value);
            cMeds = closeSetByPoint(meds, totalFeatures.value);
            cFoods = closeSetByPoint(foods, totalFeatures.value);
            cHotels = closeSetByPoint(hotels, totalFeatures.value);
            cRecs = closeSetByPoint(recs, totalFeatures.value);
            cEdus = closeSetByPoint(eduServs, totalFeatures.value);
            cFins = closeSetByPoint(finances, totalFeatures.value);
            cManufacts = closeSetByPoint(manufacts, totalFeatures.value);
            cContracts = closeSetByPoint(contracts, totalFeatures.value);
            cOthers = closeSetByPoint(others, totalFeatures.value);
         }
         else {
            cHistorics = closeSetByPoint(historics, 1);
            cRecCents = closeSetByPoint(recCents, 1);
            cMuses = closeSetByPoint(muses, 1);
            cLibs = closeSetByPoint(libs, 1);
            cPosts = closeSetByPoint(posts, 1);
            cAirs = closeSetByPoint(airs, 1);
            cTrains = closeSetByPoint(trains, 1);
            cGraves = closeSetByPoint(graves, 1);
            cFStats = closeSetByPoint(fStats, 1);
            cPStats = closeSetByPoint(pStats, 1);
            cESchools = closeSetByPoint(eSchools, 1);
            cMSchools = closeSetByPoint(mSchools, 1);
            cHSchools = closeSetByPoint(hSchools, 1);
            cHighEds = closeSetByPoint(highEds, 1);
            cSpecSchools = closeSetByPoint(specSchools, 1);
            cPreSchools = closeSetByPoint(preSchools, 1);
            cPrivSchools = closeSetByPoint(privSchools, 1);
            cParks = closeCenterByNum(parks, 1);
            cGolfs = closeSetByPoint(golfs, 1);
            cTrails = closeCenterByNum(trails, 1);
            cBus = closeSetByPoint(busResults, 1);
            cBeauts = closeSetByPoint(beauts, 1);
            cRepairs = closeSetByPoint(repairs, 1);
            cMerches = closeSetByPoint(merches, 1);
            cShops = closeSetByPoint(shops, 1);
            cMeds = closeSetByPoint(meds, 1);
            cFoods = closeSetByPoint(foods, 1);
            cHotels = closeSetByPoint(hotels, 1);
            cRecs = closeSetByPoint(recs, 1);
            cEdus = closeSetByPoint(eduServs, 1);
            cFins = closeSetByPoint(finances, 1);
            cManufacts = closeSetByPoint(manufacts, 1);
            cContracts = closeSetByPoint(contracts, 1);
            cOthers = closeSetByPoint(others, 1);
         }
      }

      if (historic.checked && !historic.disabled && cHistorics != null && cHistorics != undefined) {
         dojo.fx.wipeIn({
            node: historicRow,
            duration: 20
         }).play();
         historicRow.class = 'isDisplayed';
         if (cHistorics.length == 0) {
            dojo.fx.wipeOut({
               node: historicRow,
               duration: 20
            }).play();
            historicRow.class = 'notDisplayed';
         }
         else {
            var params = {
               features: [],
               type: "Historic Site",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cHistorics.length; i ++)
               params.features.push(cHistorics[i]);
            var str = buildTableRow(params);
            for (var i = 0; i < cHistorics.length; i++) {
               var graphic = cHistorics[i].features;
               graphic.setSymbol(historSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr('historicRow', 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: historicRow,
            duration: 20
         }).play();
         historicRow.class = 'notDisplayed';
      }

      if (recCent.checked && !recCent.disabled && cRecCents != null && cRecCents != undefined) {
         dojo.fx.wipeIn({
            node: recCentRow,
            duration: 20
         }).play();
         recCentRow.class = 'isDisplayed';
         if (cRecCents.length == 0) {
            dojo.fx.wipeOut({
               node: recCentRow,
               duration: 20
            }).play();
            recCentRow.class = 'notDisplayed';
         }
         else {
            var params = {
               features: [],
               type: "Recreation Center",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cRecCents.length; i++)
               params.features.push(cRecCents[i]);
            var str = buildTableRow(params)

            for (var i = 0; i < cRecCents.length; i++) {
               var graphic = cRecCents[i].features;
               graphic.setSymbol(recCentSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(recCentRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: recCentRow,
            duration: 20
         }).play();
         recCentRow.class = 'notDisplayed';
      }

      if (muse.checked && !muse.disabled && cMuses != null && cMuses != undefined) {
         dojo.fx.wipeIn({
            node: museRow,
            duration: 20
         }).play();
         museRow.class = 'isDisplayed';
         if (cMuses.length == 0) {
            dojo.fx.wipeOut({
               node: museRow,
               duration: 20
            }).play();
            museRow.class = 'notDisplayed';
         }
         else {
            var params = {
               features: [],
               type: "Museum",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cMuses.length; i++)
               params.features.push(cMuses[i]);

            var str = buildTableRow(params);

            for (var i = 0; i < cMuses.length; i++) {
               var graphic = cMuses[i].features;
               graphic.setSymbol(museSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(museRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: museRow,
            duration: 20
         }).play();
         museRow.class = 'notDisplayed';
      }

      if (lib.checked && !lib.disabled && cLibs != null && cLibs != undefined) {
         dojo.fx.wipeIn({
            node: libRow,
            duration: 20
         }).play();
         libRow.class = 'isDisplayed';
         if (cLibs.length == 0) {
            dojo.fx.wipeOut({
               node: libRow,
               duration: 20
            }).play();
            libRow.class = 'notDisplayed';
         }
         else {
            var params = {
               features: [],
               type: "Library",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cLibs.length; i++)
               params.features.push(cLibs[i]);

            var str = buildTableRow(params);

            for (var i = 0; i < cLibs.length; i++) {
               var graphic = cLibs[i].features;
               graphic.setSymbol(libSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(libRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: libRow,
            duration: 20
         }).play();
         libRow.class = 'notDisplayed';
      }

      if (post.checked && !post.disabled && cPosts != null && cPosts != undefined) {
         dojo.fx.wipeIn({
            node: postRow,
            duration: 20
         }).play();
         if (cPosts.length == 0)
            dojo.fx.wipeOut({
               node: postRow,
               duration: 20
            }).play();
         else {
           var params = {
               features: [],
               type: "Post Office",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cPosts.length; i ++)
               params.features.push(cPosts[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cPosts.length; i++) {
               var graphic = cPosts[i].features;
               graphic.setSymbol(postSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(postRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: postRow,
            duration: 20
         }).play();
      }


      if (air.checked && !air.disabled && cAirs != null && cAirs != undefined) {
         dojo.fx.wipeIn({
            node: airRow,
            duration: 20
         }).play();
         if (cAirs.length == 0)
            dojo.fx.wipeOut({
               node: airRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Airport",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cAirs.length; i++)
               params.features.push(cAirs[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cAirs.length; i++) {
               var graphic = cAirs[i].features;
               graphic.setSymbol(airSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(airRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: airRow,
            duration: 20
         }).play();
      }

      if (bus.checked && !bus.disabled && cBus != null && cBus != undefined) {
         if (cBus.length > 0) {
            dojo.fx.wipeIn({
               node: busRow,
               duration: 20
            }).play();
            if (cBus.length == 0)
               dojo.fx.wipeOut({
                  node: busRow,
                  duration: 20
               }).play();
            else {
               var params = {
                  features: [],
                  type: "Bus Stop",
                  name: "DESCRIPTION",
                  address: "ADDRESS",
                  site: "http://www.danville-va.gov",
                  googleAddress: "ADDRESS"
               }
               for (var i = 0; i < cBus.length; i++)
                  params.features.push(cBus[i]);
               var str = buildTableRow(params);

               for (var i = 0; i < cBus.length; i++) {
                  var graphic = cBus[i].features;
                  graphic.setSymbol(bSSym);
                  graphic.setInfoTemplate(bSITemp);
                  searchMapLayer.add(graphic);
               }
            }
            dojo.attr(busRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: busRow,
            duration: 20
         }).play();
      }

      if (train.checked && !train.disabled && cTrains != null && cTrains != undefined) {
         dojo.fx.wipeIn({
            node: trainRow,
            duration: 20
         }).play();
         if (cTrains.length == 0)
            dojo.fx.wipeOut({
               node: trainRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Train Station",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cTrains.length; i++)
               params.features.push(cTrains[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cTrains.length; i++) {
               var graphic = cTrains[i].features;
               graphic.setSymbol(trainSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(trainRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: trainRow,
            duration: 20
         }).play();
      }

      if (grave.checked && !grave.disabled && cGraves != null && cGraves != undefined) {
         dojo.fx.wipeIn({
            node: graveRow,
            duration: 20
         }).play();
         if (cGraves.length == 0)
            dojo.fx.wipeOut({
               node: graveRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Cemetery",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cGraves.length; i++)
               params.features.push(cGraves[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cGraves.length; i++) {
               var graphic = cGraves[i].features;
               graphic.setSymbol(graveSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(graveRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: graveRow,
            duration: 20
         }).play();
      }
      
      if (fStat.checked && !fStat.disabled && cFStats != null && cFStats != undefined) {
         dojo.fx.wipeIn({
            node: "fStatRow",
            duration: 20
         }).play();
         if (cFStats.length == 0)
            dojo.fx.wipeOut({
               node: fStatRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Fire Station",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cFStats.length; i++)
               params.features.push(cFStats[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cFStats.length; i++) {
               var graphic = cFStats[i].features;
               graphic.setSymbol(fSSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(fStatRow, 'innerHTML', str);
      }
      
      else {
         dojo.fx.wipeOut({
            node: fStatRow,
            duration: 20
         }).play();
      }

      if (pStat.checked && !pStat.disabled && cPStats != null && cPStats != undefined) {
         dojo.fx.wipeIn({
            node: pStatRow,
            duration: 20
         }).play();
         if (cPStats.length == 0)
            dojo.fx.wipeOut({
               node: pStatRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Police Station",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cPStats.length; i++)
               params.features.push(cPStats[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cPStats.length; i++) {
               var graphic = cPStats[i].features;
               graphic.setSymbol(pSSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(pStatRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: pStatRow,
            duration: 20
         }).play();
      }

      if (eSchool.checked && !eSchool.disabled && cESchools != null && cESchools != undefined) {
         dojo.fx.wipeIn({
            node: eSRow,
            duration: 20
         }).play();
         if (cESchools.length == 0)
            dojo.fx.wipeOut({
               node: eSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Elementary School",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cESchools.length; i++)
               params.features.push(cESchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cESchools.length; i++) {
               var graphic = cESchools[i].features;
               graphic.setSymbol(eSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(eSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: eSRow,
            duration: 20
         }).play();
      }

      if (mSchool.checked && !mSchool.disabled && cMSchools != null && cMSchools != undefined) {
         dojo.fx.wipeIn({
            node: mSRow,
            duration: 20
         }).play();
         if (cMSchools.length == 0)
            dojo.fx.wipeOut({
               node: mSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Middle School",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cMSchools.length; i++)
               params.features.push(cMSchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cMSchools.length; i++) {
               var graphic = cMSchools[i].features;
               graphic.setSymbol(mSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(mSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: mSRow,
            duration: 20
         }).play();
      }

      if (hSchool.checked && !hSchool.disabled && cHSchools != null && cHSchools != undefined) {
         dojo.fx.wipeIn({
            node: hSRow,
            duration: 20
         }).play();
         if (cHSchools.length == 0)
            dojo.fx.wipeOut({
               node: hSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "High School",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cHSchools.length; i++)
               params.features.push(cHSchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cHSchools.length; i++) {
               var graphic = cHSchools[i].features;
               graphic.setSymbol(hSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(hSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: hSRow,
            duration: 20
         }).play();
      }

      if (highEd.checked && !highEd.disabled && cHighEds != null && cHighEds != undefined) {
         dojo.fx.wipeIn({
            node: highEdRow,
            duration: 20
         }).play();
         if (cHighEds.length == 0)
            dojo.fx.wipeOut({
               node: highEdRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Higher Education",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cHighEds.length; i++)
               params.features.push(cHighEds[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cHighEds.length; i++) {
               var graphic = cHighEds[i].features;
               graphic.setSymbol(highEdSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(highEdRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: highEdRow,
            duration: 20
         }).play();
      }

      if (specSchool.checked && !specSchool.disabled && cSpecSchools != null && cSpecSchools != undefined) {
         dojo.fx.wipeIn({
            node: specSRow,
            duration: 20
         }).play();
         if (cSpecSchools.length == 0)
            dojo.fx.wipeOut({
               node: specSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Specialty School",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cSpecSchools.length; i++)
               params.features.push(cSpecSchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cSpecSchools.length; i++) {
               var graphic = cSpecSchools[i].features;
               graphic.setSymbol(specSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(specSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: specSRow,
            duration: 20
         }).play();
      }

      if (preSchool.checked && !preSchool.disabled && cPreSchools != null && cPreSchools != undefined) {
         dojo.fx.wipeIn({
            node: preSRow,
            duration: 20
         }).play();
         if (cPreSchools.length == 0)
            dojo.fx.wipeOut({
               node: preSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Preschool",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cPreSchools.length; i++)
               params.features.push(cPreSchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cPreSchools.length; i++) {
               var graphic = cPreSchools[i].features;
               graphic.setSymbol(preSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(preSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: preSRow,
            duration: 20
         }).play();
      }

      if (privSchool.checked && !privSchool.disabled && cPrivSchools != null && cPrivSchools != undefined) {
         dojo.fx.wipeIn({
            node: privSRow,
            duration: 20
         }).play();
         if (cPrivSchools.length == 0)
            dojo.fx.wipeOut({
               node: privSRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Private School",
               name: "SCHOOL",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cPrivSchools.length; i++)
               params.features.push(cPrivSchools[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cPrivSchools.length; i++) {
               var graphic = cPrivSchools[i].features;
               graphic.setSymbol(privSSym);
               graphic.setInfoTemplate(schITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(privSRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: privSRow,
            duration: 20
         }).play();
      }

      if (park.checked && !park.disabled && cParks != null && cParks != undefined) {
         dojo.fx.wipeIn({
            node: parkRow,
            duration: 20
         }).play();
         if (cParks.length == 0)
            dojo.fx.wipeOut({
               node: parkRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Park",
               name: "Name",
               address: null,
               site: "http://www.danville-va.gov",
               googleAddress: "Name"
            }
            for (var i = 0; i < cParks.length; i++)
               params.features.push(cParks[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cParks.length; i++) {
               var graphic = cParks[i].features;
               graphic.setSymbol(pSym);
               graphic.setInfoTemplate(pITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(parkRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: parkRow,
            duration: 20
         }).play();
      }

      if (golf.checked && !golf.disabled && cGolfs != null && cGolfs != undefined) {
         dojo.fx.wipeIn({
            node: golfRow,
            duration: 20
         }).play();
         if (cGolfs.length == 0)
            dojo.fx.wipeOut({
               node: golfRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Golf Course",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cGolfs.length; i++)
               params.features.push(cGolfs[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cGolfs.length; i++) {
               var graphic = cGolfs[i].features;
               graphic.setSymbol(golfSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(golfRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: golfRow,
            duration: 20
         }).play();
      }

      if (trail.checked && !trail.disabled && cTrails != null && cTrails != undefined) {
         dojo.fx.wipeIn({
            node: trailRow,
            duration: 20
         }).play();
         if (cTrails.length == 0)
            dojo.fx.wipeOut({
               node: trailRow,
               duration: 20
            }).play();
         else {
            var params = {
               features: [],
               type: "Trail",
               name: "TRAIL_NAME",
               address: null,
               site: "http://www.danville-va.gov",
               googleAddress: "TRAIL_NAME"
            }
            for (var i = 0; i < cTrails.length; i++)
               params.features.push(cTrails[i]);
            var str = buildTableRow(params);
            for (var i = 0; i < cTrails.length; i++) {
               graphic = cTrails[i].features;
               graphic.setSymbol(mTrailSym);
               if (cTrails[i].serv["TRAIL_TYPE"] == "Bike Off Road")
                  graphic.setSymbol(bTrailSym);
               if (cTrails[i].serv["TRAIL_TYPE"] == "Hiking")
                  graphic.setSymbol(hTrailSym);
               graphic.setInfoTemplate(trailITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(trailRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: trailRow,
            duration: 20
         }).play();
      }
      if (trash.checked && !trash.disabled) {
         dojo.fx.wipeIn({
            node: trashRow,
            duration: 20
         }).play();

         var str = "";

         cTrash = closeByOutline(trashes);
         if (cTrash != undefined)
            str = '<td><strong>Trash Pickup</strong></td><td><a style="padding-left: 30px">Day of Pickup:</a></td><td><a style="padding-left: 30px">' + cTrash.serv["DAY"] + '</a></td><td></td>';
         dojo.attr(trashRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: trashRow,
            duration: 20
         }).play();
      }

      if (beauty.checked && !beauty.disabled && cBeauts != null && cBeauts != undefined) {
         if (cBeauts.length > 0) {
            dojo.fx.wipeIn({
               node: beautyRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Beauty/Barber",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cBeauts.length; i++)
               params.features.push(cBeauts[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cBeauts.length; i++) {
               graphic = cBeauts[i].features;
               graphic.setSymbol(beautySym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(beautyRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: beautyRow,
            duration: 20
         }).play();
      }

      if (repair.checked && !repair.disabled && cRepairs != null && cRepairs != undefined) {
         if (cRepairs.length > 0) {
            dojo.fx.wipeIn({
               node: repairRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Repair",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cRepairs.length; i++)
               params.features.push(cRepairs[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cRepairs.length; i++) {
               graphic = cRepairs[i].features;
               graphic.setSymbol(repairSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(repairRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: repairRow,
            duration: 20
         }).play();
      }

      if (retail.checked && !retail.disabled && cMerches != null && cMerches != undefined) {
         if (cMerches.length > 0) {
            dojo.fx.wipeIn({
               node: retailRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Retail",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cMerches.length; i++)
               params.features.push(cMerches[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cMerches.length; i++) {
               graphic = cMerches[i].features;
               graphic.setSymbol(retailSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(retailRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: retailRow,
            duration: 20
         }).play();
      }

      if (shop.checked && !shop.disabled && cShops != null && cShops != undefined) {
         dojo.fx.wipeIn({
            node: shopRow,
            duration: 20
         }).play();
         shopRow.class = 'isDisplayed';
         if (cShops.length == 0) {
            dojo.fx.wipeOut({
               node: shopRow,
               duration: 20
            }).play();
            shopRow.class = 'notDisplayed';
         }
         else {
            var params = {
               features: [],
               type: "Shopping Center",
               name: "NAME",
               address: "ADDRESS",
               site: "http://www.danville-va.gov",
               googleAddress: "ADDRESS"
            }
            for (var i = 0; i < cShops.length; i++)
               params.features.push(cShops[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cShops.length; i++) {
               var graphic = cShops[i].features;
               graphic.setSymbol(shopSym);
               graphic.setInfoTemplate(servITemp);
               searchMapLayer.add(graphic);
            }
         }
         dojo.attr(shopRow, 'innerHTML', str);
      }

      else {
         dojo.fx.wipeOut({
            node: shopRow,
            duration: 20
         }).play();
         shopRow.class = 'notDisplayed';
      }



      if (med.checked && !med.disabled && cMeds != null && cMeds != undefined) {
         if (cMeds.length > 0) {
            dojo.fx.wipeIn({
               node: medRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Health Care",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cMeds.length; i++)
               params.features.push(cMeds[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cMeds.length; i++) {
               graphic = cMeds[i].features;
               graphic.setSymbol(medSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(medRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: medRow,
            duration: 20
         }).play();
      }

      if (food.checked && !food.disabled && cFoods != null && cFoods != undefined) {
         if (cFoods.length > 0) {
            dojo.fx.wipeIn({
               node: foodRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Restaurant",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cFoods.length; i++)
               params.features.push(cFoods[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cFoods.length; i++) {
               graphic = cFoods[i].features;
               graphic.setSymbol(foodSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(foodRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: foodRow,
            duration: 20
         }).play();
      }

      if (hotel.checked && !hotel.disabled && cHotels != null && cHotels != undefined) {
         if (cHotels.length > 0) {
            dojo.fx.wipeIn({
               node: hotelRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Room/Board",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cHotels.length; i++)
               params.features.push(cHotels[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cHotels.length; i++) {
               graphic = cHotels[i].features;
               graphic.setSymbol(hotelSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(hotelRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: hotelRow,
            duration: 20
         }).play();
      }

      if (rec.checked && !rec.disabled && cRecs != null && cRecs != undefined) {
         if (cRecs.length > 0) {
            dojo.fx.wipeIn({
               node: recRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Recreation/Leisure",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cRecs.length; i++)
               params.features.push(cRecs[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cRecs.length; i++) {
               graphic = cRecs[i].features;
               graphic.setSymbol(recSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(recRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: recRow,
            duration: 20
         }).play();
      }

      if (eduServ.checked && !eduServ.disabled && cEdus != null && cEdus != undefined) {
         if (cEdus.length > 0) {
            dojo.fx.wipeIn({
               node: eduServRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Educational Services",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cEdus.length; i++)
               params.features.push(cEdus[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cEdus.length; i++) {
               graphic = cEdus[i].features;
               graphic.setSymbol(eduSSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(eduServRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: eduServRow,
            duration: 20
         }).play();
      }



      if (finServ.checked && !finServ.disabled && cFins != null && cFins != undefined) {
         if (cFins.length > 0) {
            dojo.fx.wipeIn({
               node: finServRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Financial Services",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cFins.length; i++)
               params.features.push(cFins[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cFins.length; i++) {
               graphic = cFins[i].features;
               graphic.setSymbol(finSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(finServRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: finServRow,
            duration: 20
         }).play();
      }



      if (manufact.checked && !manufact.disabled && cManufacts != null && cManufacts != undefined) {
         if (cManufacts.length > 0) {
            dojo.fx.wipeIn({
               node: manufactRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Manufacturing",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cManufacts.length; i++)
               params.features.push(cManufacts[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cManufacts.length; i++) {
               graphic = cManufacts[i].features;
               graphic.setSymbol(manufactSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(manufactRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: manufactRow,
            duration: 20
         }).play();
      }



      if (contract.checked && !contract.disabled && cContracts != null && cContracts != undefined) {
         if (cContracts.length > 0) {
            dojo.fx.wipeIn({
               node: contractRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Contractor",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cContracts.length; i++)
               params.features.push(cContracts[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cContracts.length; i++) {
               graphic = cContracts[i].features;
               graphic.setSymbol(contractSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(contractRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: contractRow,
            duration: 20
         }).play();
      }



      if (other.checked && !other.disabled && cOthers != null && cOthers != undefined) {
         if (cOthers.length > 0) {
            dojo.fx.wipeIn({
               node: otherRow,
               duration: 20
            }).play();

            var params = {
               features: [],
               type: "Other Services",
               name: "AccountNam",
               address: "Location",
               site: "http://www.danville-va.gov",
               googleAddress: "Location"
            }
            for (var i = 0; i < cOthers.length; i++)
               params.features.push(cOthers[i]);
            var str = buildTableRow(params);

            for (var i = 0; i < cOthers.length; i++) {
               graphic = cOthers[i].features;
               graphic.setSymbol(otherSym);
               graphic.setInfoTemplate(busITemp);
               searchMapLayer.add(graphic);
            }
            dojo.attr(otherRow, 'innerHTML', str);
         }
      }
      else {
         dojo.fx.wipeOut({
            node: otherRow,
            duration: 20
         }).play();
      }

      if (cBlock.checked && !cBlock.disabled) {
         dojo.fx.wipeIn({
            node: cBlockRow,
            duration: 20
         }).play();

         cCBlock = closeByOutline(cBlocks);
         var str = "";
         if (cBlocks != null && cBlocks != undefined)
            str = '<td><strong>Census Block</strong></td><td><a href="http://www.danville-va.gov" target="_blank">' + cCBlock.serv["NAME10"] + '<a><td></td></td>';
         dojo.attr(cBlockRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: cBlockRow,
            duration: 20
         }).play();
      }

      if (eZone.checked && !eZone.disabled) {
         dojo.fx.wipeIn({
            node: eZoneRow,
            duration: 20
         }).play();

         cEntZone = closeByOutline(eZones);
         var str = "";
         if (cEntZone != null && cEntZone != undefined)
            str = '<td><strong>Enterprise Zone</strong></td><td><a href="http://www.danville-va.gov" target="_blank">' + cEntZone.serv["CLASS"] + '<a><td></td></td>';
         dojo.attr(eZoneRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: eZoneRow,
            duration: 20
         }).play();
      }

      if (fZone.checked && !fZone.disabled) {
         dojo.fx.wipeIn({
            node: fZoneRow,
            duration: 20
         }).play();

         cFlood = inFeature(floods);
         var str = "";
         if (cFlood != null && cFlood != undefined)
            str = '<td><strong>Flood Zone</strong></td><td>&emsp;</td><td><a href="http://www.danville-va.gov" target="_blank">' + cFlood.serv["FLD_ZONE"] + '<a><td></td></td>';
         dojo.attr(fZoneRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: fZoneRow,
            duration: 20
         }).play();
      }
      if (hDist.checked && !hDist.disabled) {
         dojo.fx.wipeIn({
            node: hDistRow,
            duration: 20
         }).play();

         cHist = closeByOutline(hists);
         var str = "";
         if (cHist != null && cHist != undefined)
            str = '<td><strong>Historical District</strong></td><td><a href="http://www.danville-va.gov" target="_blank">' + cHist.serv["DESCR"] + '<a><td></td></td>';
         dojo.attr(hDistRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: hDistRow,
            duration: 20
         }).play();
      }
      if (zone.checked && !zone.disabled) {
         dojo.fx.wipeIn({
            node: zoneRow,
            duration: 20
         }).play();

         cZone = closeByOutline(zones);
         var str = "";
         if (cZone != null && cZone != undefined)
            str = '<td><strong>Zoning</strong></td><td><a href="http://www.danville-va.gov" target="_blank">' + cZone.serv["ZONE"] + '</a></td><td> ' + cZone.serv["ORDINANCE"] + '</td>';
         dojo.attr(zoneRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: zoneRow,
            duration: 20
         }).play();
      }

      if (hubZone.checked && !hubZone.disabled) {
         dojo.fx.wipeIn({
            node: hubRow,
            duration: 20
         }).play();

         cHub = closeByOutline(hubs);
         var str = "";
         if (cHub != null && cHub != undefined)
            str = '<td><strong>Hub Zone</strong></td><td><a href="http://www.danville-va.gov" target="_blank">' + cHub.serv["TRACT"] + '<a><td></td></td>';
         dojo.attr(hubRow, 'innerHTML', str);
      }
      else {
         dojo.fx.wipeOut({
            node: hubRow,
            duration: 20
         }).play();
      }
      if (wanted) {
         var graphic = new esri.Graphic(point, wantSym, {}, wantITemp);
         searchMapLayer.add(graphic);
         map.infoWindow.setContent(graphic.getContent());
         map.infoWindow.setTitle(graphic.getTitle());
         map.infoWindow.show(point);
      }
   }
   else {
      searchMapLayer.clear();
      idparams.mapExtent = map.extent;
      idparams.geometry = map.extent;
      identify.execute(idparams, pushIdResults);
   }
   setTableColoring();
}

function searchMap(evt) {
   if (searchTxt.textbox.value != null && searchTxt.textbox.value != "") {
      var text = searchTxt.textbox.value;
      params.layerIds = [];
      if (serv.checked) {
         if (edu.checked) {
            params.layerIds.push(13);
            params.layerIds.push(14);
            params.layerIds.push(15);
            params.layerIds.push(16);
            params.layerIds.push(17);
            params.layerIds.push(18);
            params.layerIds.push(19);
         }
         params.layerIds.push(1);
         params.layerIds.push(2);
         params.layerIds.push(3);
         params.layerIds.push(4);
         params.layerIds.push(5);
         params.layerIds.push(6);
         params.layerIds.push(7);
         params.layerIds.push(8);
         params.layerIds.push(9);
         params.layerIds.push(10);
         params.layerIds.push(11);
         params.layerIds.push(20);
         params.layerIds.push(21);
         params.layerIds.push(22);
      }
      if (business.checked) {
         params.layerIds.push(25);
         params.layerIds.push(26);
         params.layerIds.push(27);
         params.layerIds.push(28);
         params.layerIds.push(29);
         params.layerIds.push(30);
         params.layerIds.push(31);
         params.layerIds.push(32);
         params.layerIds.push(33);
         params.layerIds.push(34);
         params.layerIds.push(35);
         params.layerIds.push(36);
         params.layerIds.push(37);
      }

      params.searchText = text;
      searched = true;
      find.execute(params, function (results) { results.length > 0 ? findResults(results) : locateResults(text); });
      searchTxt.set('value', "");
   }
}

function findResults(results) {
   map.graphics.clear();
   var symbol = new esri.symbol.PictureMarkerSymbol('red-marker.png', 30, 30);
   var graphic;
   dojo.forEach(results, function (result) {
      if (result.geometryType == "esriGeometryPoint")
         graphic = result.feature;
      if (result.geometryType == "esriGeometryPolygon" || result.geometryType == "esriGeometryPolyline") {
         graphic = result.feature;
         graphic.setGeometry(result.feature.geometry.getExtent().getCenter());
      }
      graphic.setSymbol(symbol);
      switch (result.layerId) {
         case 1:
            graphic.setInfoTemplate(servITemp);
            break;
         case 2:
            graphic.setInfoTemplate(servITemp);
            break;
         case 3:
            graphic.setInfoTemplate(servITemp);
            break;
         case 4:
            graphic.setInfoTemplate(servITemp);
            break;
         case 5:
            graphic.setInfoTemplate(servITemp);
            break;
         case 6:
            graphic.setInfoTemplate(servITemp);
            break;
         case 7:
            graphic.setInfoTemplate(bSITemp);
            break;
         case 8:
            graphic.setInfoTemplate(servITemp);
            break;
         case 9:
            graphic.setInfoTemplate(servITemp);
            break;
         case 10:
            graphic.setInfoTemplate(servITemp);
         case 11:
            graphic.setInfoTemplate(servITemp);
            break;
         case 12:
            graphic.setInfoTemplate(busITemp);
            break;
         case 13:
            graphic.setInfoTemplate(schITemp);
            break;
         case 14:
            graphic.setInfoTemplate(schITemp);
            break;
         case 15:
            graphic.setInfoTemplate(schITemp);
            break;
         case 16:
            graphic.setInfoTemplate(schITemp);
            break;
         case 17:
            graphic.setInfoTemplate(schITemp);
            break;
         case 18:
            graphic.setInfoTemplate(schITemp);
            break;
         case 19:
            graphic.setInfoTemplate(schITemp);
            break;
         case 20:
            graphic.setInfoTemplate(pITemp);
            break;
         case 21:
            graphic.setInfoTemplate(servITemp);
            break;
         case 22:
            graphic.setInfoTemplate(trailITemp);
            break;
         case 25:
            graphic.setInfoTemplate(busITemp);
            break;
         case 26:
            graphic.setInfoTemplate(busITemp);
            break;
         case 27:
            graphic.setInfoTemplate(busITemp);
            break;
         case 28:
            graphic.setInfoTemplate(busITemp);
            break;
         case 29:
            graphic.setInfoTemplate(busITemp);
            break;
         case 30:
            graphic.setInfoTemplate(busITemp);
            break;
         case 31:
            graphic.setInfoTemplate(busITemp);
            break;
         case 32:
            graphic.setInfoTemplate(busITemp);
            break;
         case 33:
            graphic.setInfoTemplate(busITemp);
            break;
         case 34:
            graphic.setInfoTemplate(busITemp);
            break;
         case 35:
            graphic.setInfoTemplate(busITemp);
            break;
         case 36:
            graphic.setInfoTemplate(busITemp);
            break;
         case 37:
            graphic.setInfoTemplate(busITemp);
            break;
         default:
            graphic.setInfoTemplate();
            break;
      }
      map.graphics.add(graphic);
   });
   map.infoWindow.setTitle(results[0].feature.getTitle());
   map.infoWindow.setContent(results[0].feature.getContent());
   map.infoWindow.show(graphic.geometry);
   map.centerAt(graphic.geometry);
}

function locateResults(searchTxt) {
   var address = {
      "Street": searchTxt,
      "City": "",
      "State": "",
      "ZIP": "",
      "f": "pjson"
   };
   lparams = { address: address, outFields: locOutFields };
   locate.addressToLocations(lparams, showLocate, errLocate);
}

function errLocate(result) {
   console.log("error accessing the geocoding service");
   clearSearch();
}

function showLocate(results) {
   map.graphics.clear();
   if (results.length > 0) {
      var symbol = new esri.symbol.PictureMarkerSymbol('red-marker.png', 30, 30);
      var graphic;
      for (var i = 0, l = results.length; i < l; i++) {
         locateITemp = new esri.InfoTemplate(results[i].address);
         graphic = new esri.Graphic();
         graphic.setGeometry(results[i].location);
         graphic.setInfoTemplate(locateITemp);
         graphic.setSymbol(symbol);
         map.graphics.add(graphic);
         map.infoWindow.setTitle("<br/>");
         map.infoWindow.setContent(results[0].address);
         map.infoWindow.show(results[0].location);
      }
      map.centerAt(results[0].location);
   }
   else {
      clearSearch();
   }
}

function clearSearch() {
   searched = false;
   wanted = false;
   map.infoWindow.hide();
   map.graphics.clear();
}

function clearInfo() {
   if (wanted) {
      wanted = false;
   };
   wantTo.set('item', null);
}

function calcDist(point1, point2) {
   var xdist = Math.abs(point1.x - point2.x);
   var ydist = Math.abs(point1.y - point2.y);
   var tdist = Math.sqrt((xdist * xdist) + (ydist * ydist));
   return tdist;
}

function buildTableRow(params) {
   var str = '<td align="left" valign="top"><strong>' + params.type + '</strong></td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0)
         str = str + '<br/>';
      if (params.features[i].serv[params.name] != null) {
         str = str + toTitleCase(params.features[i].serv[params.name].toLowerCase());
      }
      if(params.site != null && params.site!= undefined)
         str = str + ' <a href="' + params.site + '" target="_blank">website</a>';
   }
   str = str + '</td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0)
         str = str + '<br/>';
      if (params.features[i].features.geometry.type != "polygon" && params.features[i].features.geometry.type != "polyline") {
         str = str + '<a  onClick="map.centerAt(new esri.geometry.Point(' + params.features[i].features.geometry.x + ', ' + params.features[i].features.geometry.y + ', {wkid:2284}))">';
      }
      else {
         str = str + '<a onClick="map.centerAt(new esri.geometry.Point(' + params.features[i].features.geometry.getExtent().getCenter().x + ', ' + params.features[i].features.geometry.getExtent().getCenter().y + ', {wkid:2284}))">';
      } 
      if (params.address != null) {
         str = str + toTitleCase(params.features[i].serv[params.address].toLowerCase()) + '</a>';
      }
      else {
         if (params.add1 === undefined) {
            if (params.type === "Park")
               str = str + 'Go To Park</a>';
            if (params.type === "Trail")
               str = str + 'Go To Trail</a>';
         }
         else
            str = str + toTitleCase(params.features[i].serv[params.add1].toLowerCase()) + ', ' + toTitleCase(params.features[i].serv[params.add2].toLowerCase()) + '</a>';
      }
   }
   str = str + '</td><td>';
   for (var i = 0; i < params.features.length; i++) {
      if (i > 0)
         str = str + '<br/>';
      str = str + '<a href="http://maps.google.com/maps?daddr=' + params.features[i].serv[params.googleAddress] + ', Danville, VA&hl=en" target="_blank">' + params.features[i].dist + ' Miles</a>';
   }
   str = str + '</td>';

   return str;
}

function setTableColoring() {
   var counter = 1;
   for (var i = 0; i < rowList.length; i++) {
      if (rowList[i].class === "isDisplayed") {
         if (counter % 2 == 0)
            rowList[i].style = "background-color: white";
         else
            rowList[i].style = "background-color: lightgrey";
         counter = counter + 1;
      }
   }
}


dojo.ready(init);
dojo.ready(executeQueryTask);